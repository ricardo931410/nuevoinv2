<?php
$alert = '';
session_start();
if (!empty ($_SESSION['active'])) {
  header('location: sistema/ ');
}else{
  
  if (!empty($_POST)) 
  {
    if (empty($_POST['email']) || empty($_POST['clave']))
    {
      $alert = 'ingrese su usuario y su contraseña';
    }else{
      require "conexion.php";

      $email = $_POST['email'];
      $pass = $_POST['clave'];

      $consulta = "SELECT * FROM Usuario WHERE Email = '$email'";

      $resultado = $conexion -> query($consulta);

      $num = $resultado -> num_rows;

      if ($num>0) {
        
        $row = $resultado -> fetch_assoc();
        $passdb = $row['Clave'];
        if ($passdb == $pass) {

          $_SESSION['active']     = TRUE;
          $_SESSION['idUsuario']  = $row['idUsuario'];
          $_SESSION['nombre']     = $row['Nombre'];
          $_SESSION['apellido']   = $row['Apellido'];
          $_SESSION['email']      = $row['Email'];
          $_SESSION['rol']        = $row['idRol'];
          
          header('location: sistema/');
        }else{
          $alert = 'el usuario y la contraseña es incorrecto';
        session_destroy();
        }
      }else{
        $alert = 'el usuario no existe';
      }


/*       $email = mysqli_real_escape_string($conexion,$_POST['email']);
      $pass = md5(mysqli_real_escape_string($conexion,$_POST['clave'])); */
      /* $records = $conexion -> prepare('SELECT * FROM Usuario WHERE Email = :email');
      $records ->bind_param('email',$_POST['email']);
      $records -> execute();

      $results = $records -> fetch(PDO::FETCH_ASSOC);
      $message='';

      if (is_countable($results) > 0 && password_verify($_POST['clave'], $results['Clave'])) {
        $_SESSION['active']     = TRUE;
        $_SESSION['idUsuario']  = $results['idUsuario'];
        $_SESSION['nombre']     = $results['Nombre'];
        $_SESSION['apellido']   = $results['Apellido'];
        $_SESSION['email']      = $results['Email'];
        $_SESSION['rol']        = $results['idRol'];

        header('location: sistema/');
      }else{
        $alert = 'el usuario y la contraseña es incorrecto';
        session_destroy();
      } */
/* ---------------------------------------------------------------------------------- */
      /* $email = $_POST['email'];
      $pass = $_POST['clave'];
      
      $consulta = "SELECT * FROM Usuario WHERE Email = '$email' AND Clave = '$pass'";
      $resultado = mysqli_query($conexion,$consulta); */
      /* mysqli_close($conexion); */
      /* $filas = mysqli_num_rows($resultado); */
      /* echo $result;
      exit; */
      
    /*  if ($filas > 0) {
        $datos = mysqli_fetch_array($resultado);
        
        $_SESSION['active']     = TRUE;
        $_SESSION['idUsuario']  = $datos['idUsuario'];
        $_SESSION['nombre']     = $datos['Nombre'];
        $_SESSION['apellido']   = $datos['Apellido'];
        $_SESSION['email']      = $datos['Email'];
        $_SESSION['rol']        = $datos['idRol'];
        
        header('location: sistema/');
      }else{
        $alert = 'el usuario y la contraseña es incorrecto';
        session_destroy();
      }
      mysqli_free_result($resultado);
      mysqli_close($conexion); */
    }
  }
}
?>

<!DOCTYPE html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />
  <link rel="stylesheet" href="css/main.css">
  <title>Inicio de sesion</title>
</head>
<body>
<div class="login-page">
    <div class="text-center">
       <h1>Bienvenido</h1>
       <p>Inicia sesión </p>
     </div>
      <form method="POST" action="" class="clearfix">
        <div class="form-group">
              <label for="email" class="control-label">Correo electronico</label>
              <input type="email" class="form-control" name="email" placeholder="Correo electronico">
        </div>
        <div class="form-group">
            <label for="clave" class="control-label">Contraseña</label>
            <input type="password" name= "clave" class="form-control" placeholder="Contraseña">
        </div>
        <div class="alert"> <?php echo isset($alert) ? $alert : ''; ?>   </div>
        <div class="form-group">
                <button type="submit" class="btn btn-info  pull-right">Ingresar</button>
        </div>
    </form>
</div>
</body>
</html>