-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 24-09-2021 a las 21:50:33
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `inventary`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `acualizarlibro` (IN `cantidad` INT, IN `código` INT)  BEGIN
DECLARE nueva_existencia int;
DECLARE actual_existencia int;

SELECT Ejemplares INTO actual_existencia FROM Libro WHERE idLibro = codigo;
    
SET nueva_existencia = actual_existencia + cantidad;
    
UPDATE Libro SET Ejemplares = nueva_existencia WHERE idLibro = codigo;
    
SELECT nueva_existencia;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `add_detalle_temp` (IN `codigo` INT, IN `cantidad` INT, IN `token_user` VARCHAR(50))  BEGIN
INSERT INTO detalle_temp(token_user,idLibro,Cantidad)VALUES(token_user,codigo,cantidad);

SELECT tmp.idDetalleTempSalida,tmp.idLibro,l.Titulo,l.Coleccion,tmp.cantidad 
FROM detalle_temp tmp
INNER JOIN Libro l 
ON tmp.idLibro = l.idLibro
WHERE tmp.token_user = token_user;
END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `del_detalle_temp` (IN `id_detalle` INT, IN `token` VARCHAR(50))  BEGIN
    DELETE FROM detalle_temp WHERE idDetalleTempSalida = id_detalle;
    
    
    SELECT tmp.idDetalleTempSalida,tmp.idLibro,l.Titulo,l.Coleccion,tmp.cantidad FROM detalle_temp tmp
    INNER JOIN Libro l
    on tmp.idLibro = l.idLibro
    WHERE tmp.token_user = token;
    END$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `procesar_salida` (IN `id_usuario` INT, IN `id_don` INT, IN `token` VARCHAR(50))  BEGIN
	DECLARE salida INT;
    DECLARE registros INT;
    DECLARE total INT;
    
    DECLARE nueva_existencia int;
    DECLARE existencia_actual int;
    
    DECLARE tmp_cod_libro int;
    DECLARE tmp_cant_libro int;
    DECLARE a INT;
    SET a = 1;
    
    CREATE TEMPORARY TABLE tbl_tmp_tokenuser(
    id BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
    cod_libro BIGINT,
    cant_libro INT);
    
    SET registros = (SELECT COUNT(*) FROM detalle_temp WHERE token_user = token);
    
    IF registros > 0 THEN
    	INSERT INTO tbl_tmp_tokenuser(cod_libro,cant_libro)SELECT idLibro,Cantidad FROM detalle_temp WHERE token_user = token;
        
        INSERT INTO Salida(idUsuario,idDonatario)VALUES(id_usuario,id_don);
        SET salida = LAST_INSERT_ID();
        
        INSERT INTO DetalleSalida(idSalida,idLibro,Cantidad) SELECT (salida) as idSalida, 			idLibro,Cantidad FROM detalle_temp WHERE token_user = token;
        
        WHILE a <= registros DO
        	SELECT cod_libro, cant_libro INTO tmp_cod_libro,tmp_cant_libro FROM 					tbl_tmp_tokenuser WHERE id = a;
            
            SELECT Ejemplares INTO existencia_actual FROM Libro WHERE idLibro = 					tmp_cod_libro;
            
            SET nueva_existencia = existencia_actual - tmp_cant_libro;
            UPDATE Libro SET Ejemplares = nueva_existencia WHERE idLibro = tmp_cod_libro;
            
            SET a = a+1;
        
        END WHILE;
        
       SET total = (SELECT SUM(Cantidad) FROM detalle_temp WHERE  token_user = token);
        UPDATE Salida SET Total = total WHERE idSalida = salida;
        
        DELETE FROM detalle_temp WHERE token_user = token;
        TRUNCATE TABLE tbl_tmp_tokenuser;
        SELECT * FROM Salida WHERE idSalida = salida;
    ELSE
    SELECT 0;
    END IF;
    

END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `DetalleSalida`
--

CREATE TABLE `DetalleSalida` (
  `idDetalleSalida` int(11) NOT NULL,
  `idLibro` int(11) NOT NULL,
  `idSalida` int(11) NOT NULL,
  `Entrego` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Cantidad` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `DetalleSalida`
--

INSERT INTO `DetalleSalida` (`idDetalleSalida`, `idLibro`, `idSalida`, `Entrego`, `Cantidad`) VALUES
(1, 73, 1, NULL, 14),
(2, 74, 1, NULL, 14),
(3, 75, 1, NULL, 14),
(4, 76, 1, NULL, 14),
(5, 77, 1, NULL, 14),
(8, 269, 2, NULL, 520),
(9, 270, 2, NULL, 560),
(10, 271, 2, NULL, 550),
(11, 269, 3, NULL, 700),
(12, 270, 3, NULL, 700),
(13, 271, 3, NULL, 700),
(14, 269, 4, NULL, 1),
(15, 270, 4, NULL, 1),
(16, 271, 4, NULL, 1),
(17, 269, 5, NULL, 378),
(18, 270, 5, NULL, 378),
(19, 271, 5, NULL, 378),
(20, 269, 6, NULL, 2),
(21, 270, 6, NULL, 2),
(22, 271, 6, NULL, 2),
(23, 269, 7, NULL, 1),
(24, 270, 7, NULL, 1),
(25, 271, 7, NULL, 1),
(26, 134, 7, NULL, 1),
(27, 135, 7, NULL, 1),
(28, 136, 7, NULL, 1),
(29, 137, 7, NULL, 1),
(30, 138, 7, NULL, 1),
(31, 139, 7, NULL, 1),
(32, 140, 7, NULL, 1),
(33, 141, 7, NULL, 1),
(34, 142, 7, NULL, 1),
(35, 143, 7, NULL, 1),
(36, 144, 7, NULL, 1),
(37, 145, 7, NULL, 1),
(38, 146, 7, NULL, 1),
(39, 147, 7, NULL, 1),
(40, 148, 7, NULL, 1),
(41, 149, 7, NULL, 1),
(42, 205, 7, NULL, 1),
(43, 206, 7, NULL, 1),
(44, 207, 7, NULL, 1),
(45, 208, 7, NULL, 1),
(46, 209, 7, NULL, 1),
(47, 210, 7, NULL, 1),
(48, 211, 7, NULL, 1),
(49, 212, 7, NULL, 1),
(50, 213, 7, NULL, 1),
(51, 214, 7, NULL, 1),
(52, 215, 7, NULL, 1),
(53, 216, 7, NULL, 1),
(54, 217, 7, NULL, 1),
(55, 218, 7, NULL, 1),
(56, 219, 7, NULL, 1),
(57, 262, 7, NULL, 1),
(58, 261, 7, NULL, 1),
(59, 260, 7, NULL, 1),
(60, 267, 7, NULL, 1),
(61, 263, 7, NULL, 1),
(62, 264, 7, NULL, 1),
(63, 265, 7, NULL, 1),
(64, 266, 7, NULL, 1),
(86, 269, 8, NULL, 2),
(87, 270, 8, NULL, 2),
(88, 271, 8, NULL, 2),
(89, 260, 9, NULL, 3),
(90, 263, 9, NULL, 3),
(91, 264, 9, NULL, 3),
(92, 265, 9, NULL, 3),
(93, 266, 9, NULL, 3),
(94, 270, 9, NULL, 3),
(95, 272, 9, NULL, 3),
(96, 273, 9, NULL, 3),
(97, 275, 9, NULL, 3),
(98, 268, 9, NULL, 3),
(104, 267, 10, NULL, 1),
(105, 267, 11, NULL, 1),
(106, 267, 12, NULL, 1),
(107, 272, 13, NULL, 52),
(108, 273, 13, NULL, 52),
(109, 270, 13, NULL, 52),
(110, 264, 13, NULL, 52),
(111, 265, 13, NULL, 52),
(112, 266, 13, NULL, 52),
(114, 272, 14, NULL, 1),
(115, 273, 14, NULL, 1),
(116, 274, 14, NULL, 1),
(117, 275, 14, NULL, 1),
(118, 276, 14, NULL, 1),
(119, 277, 14, NULL, 1),
(120, 278, 14, NULL, 1),
(121, 279, 14, NULL, 1),
(122, 280, 14, NULL, 1),
(123, 271, 15, NULL, 5),
(124, 270, 15, NULL, 5),
(125, 269, 15, NULL, 5),
(126, 263, 15, NULL, 5),
(127, 264, 15, NULL, 5),
(128, 265, 15, NULL, 5),
(129, 95, 15, NULL, 5),
(130, 250, 15, NULL, 5),
(131, 251, 15, NULL, 5),
(132, 252, 15, NULL, 5),
(133, 253, 15, NULL, 5),
(134, 254, 15, NULL, 5),
(135, 255, 15, NULL, 5),
(136, 256, 15, NULL, 5),
(137, 257, 15, NULL, 5),
(138, 258, 15, NULL, 5),
(139, 259, 15, NULL, 5),
(140, 114, 15, NULL, 5),
(141, 115, 15, NULL, 5),
(142, 116, 15, NULL, 5),
(143, 117, 15, NULL, 5),
(144, 118, 15, NULL, 5),
(145, 119, 15, NULL, 5),
(146, 120, 15, NULL, 5),
(147, 121, 15, NULL, 5),
(148, 122, 15, NULL, 5),
(149, 123, 15, NULL, 5),
(150, 124, 15, NULL, 5),
(151, 125, 15, NULL, 5),
(152, 126, 15, NULL, 5),
(153, 127, 15, NULL, 5),
(154, 128, 15, NULL, 5),
(155, 129, 15, NULL, 5),
(156, 130, 15, NULL, 5),
(157, 131, 15, NULL, 5),
(158, 132, 15, NULL, 5),
(159, 133, 15, NULL, 5),
(186, 270, 16, NULL, 10),
(187, 274, 16, NULL, 10),
(188, 277, 16, NULL, 10),
(189, 267, 16, NULL, 10),
(190, 295, 16, NULL, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_temp`
--

CREATE TABLE `detalle_temp` (
  `idDetalleTempSalida` int(20) NOT NULL,
  `token_user` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `idLibro` int(11) NOT NULL,
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Donatario`
--

CREATE TABLE `Donatario` (
  `idDonatario` int(11) NOT NULL,
  `Nombre` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Contacto` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Direccion` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Telefono` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Email` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Motivo` varchar(200) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Estatus` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Donatario`
--

INSERT INTO `Donatario` (`idDonatario`, `Nombre`, `Contacto`, `Direccion`, `Telefono`, `Email`, `Motivo`, `Estatus`) VALUES
(1, 'María Antonieta González Mendoza', 'María Antonieta González Mendoza', '', '', '', 'Solicitado via telefonica.', 1),
(2, 'JUD Fomento a la lectura Alcaldía GAM', 'Francisco Garcia', '', '', '', 'Solicitud mediante oficio AGAM/DGDS/JUDFL/029/2021', 1),
(3, 'Diego Pérez Villavicencio', 'Diego Pérez Villavicencio', 'Av. Juarez s/n Cuajimalpa, Cuajimalpa de Morelos CDMX.', '', '', 'Atención al oficio AMC/DGBS/SB/273/2021', 1),
(4, ' Mtra. María Estela del Valle Guerrero ', ' Mtra. María Estela del Valle Guerrero ', 'Chapultepec 49 Centro Cuauhtemoc CDMX', '', '', 'Solicitud para proyecto transversal', 1),
(5, 'JUD. Promocion de la lectura', 'Jesús Arriaga Morales', '000', '', '', 'Solicitud de donación mediante oficio AI/DEC/JUDPL/024/2021', 1),
(6, 'Subdireccion de Educación a Distancia', 'Lic. América Martínez Frausto', '', '', '', 'Solicitud realizada para el proyecto de Talleres Extraescolares en Bibliotecas', 1),
(7, 'LCP Investigación y Capacitación DGAEA', 'Lic. Sara de la Fuente Guerrero', '', '', '', 'Centro de Atención a Mujeres Adolescentes, para entregarlo a una menor a su salida.', 1),
(8, 'Direccion de Educación Contínua', 'Lic. Martha Lluvia Sánchez ', '', '', '', 'Solicitud para la elaboración de las fichas pedagógicas', 1),
(9, 'Paulo Cesar Martinez Lopez', 'Paulo Cesar Martinez Lopez', '', '', '', 'Para entrega de premios del concurso de cortometraje', 1),
(10, 'Roberto Hazael Padilla Martinez', 'Roberto Hazael Padilla Martinez', '', '', '', 'Para participacion en la presentacion editorial', 1),
(11, 'Karen Vigueras', 'Karen Vigueras', '', '', '', 'Para participacion en la presentacion editorial', 1),
(12, 'Andrei López García', 'Andrei López García', '', '', '', 'Para participacion en la presentacion editorial', 1),
(13, 'Sara C. de la Fuente Guerrero', 'Sara C. de la Fuente Guerrero', '', '', '', 'Entrega en la presentacion editorial Mi casa bajo la noche ', 1),
(14, 'Rafael Cessa Flores  ', 'Rafael Cessa Flores  ', '  ', '  ', '  ', 'Entrega para revisión y establecer estrategias de difusión de acervo', 1),
(15, 'Daniel Camacho', 'Daniel Camacho', 'Nezahualcóyotl 127, Col. Centro, Alcaldía Cuauhtémoc', '', '', 'Solicitados por Daniel Camacho', 1),
(16, 'Realizadores del mural', 'Realizadores del mural', '', '', '', 'Entrega de libros a realizadores del mural en biblioteca Juventino Rosas', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Entrada`
--

CREATE TABLE `Entrada` (
  `idEntrada` int(11) NOT NULL,
  `idLibro` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `Recibe` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Adquisicion` varchar(10) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `Cantidad` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Entrada`
--

INSERT INTO `Entrada` (`idEntrada`, `idLibro`, `idUsuario`, `Recibe`, `Adquisicion`, `Fecha`, `Cantidad`) VALUES
(1, 1, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(2, 2, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(3, 3, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(4, 4, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(5, 5, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(6, 6, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(7, 7, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(8, 8, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(9, 9, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(10, 10, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(11, 11, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(12, 12, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(13, 13, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(14, 14, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(15, 15, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(16, 16, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(17, 17, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(18, 18, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(19, 19, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(20, 20, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(21, 21, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(22, 22, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(23, 23, 2, '', 'Donación', '2021-06-29 00:00:00', 6),
(24, 24, 2, '', 'Donación', '2021-06-29 00:00:00', 27),
(25, 25, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(26, 26, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(27, 27, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(28, 28, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(29, 29, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(30, 30, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(31, 31, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(32, 32, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(33, 33, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(34, 34, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(35, 35, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(36, 36, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(37, 37, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(38, 38, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(39, 39, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(40, 40, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(41, 41, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(42, 42, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(43, 43, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(44, 44, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(45, 45, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(46, 46, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(47, 47, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(48, 48, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(49, 49, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(50, 50, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(51, 51, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(52, 52, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(53, 53, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(54, 54, 2, '', 'Donación', '2021-06-29 00:00:00', 42),
(55, 55, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(56, 56, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(57, 57, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(58, 58, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(59, 59, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(60, 60, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(61, 61, 2, '', 'Donación', '2021-06-29 00:00:00', 1),
(62, 62, 2, '', 'Donación', '2021-06-29 00:00:00', 2),
(63, 63, 2, '', 'Donación', '2021-06-29 00:00:00', 16),
(64, 64, 2, '', 'Donación', '2021-06-29 00:00:00', 12),
(65, 65, 2, '', 'Donación', '2021-06-29 00:00:00', 9),
(66, 66, 2, '', 'Donación', '2021-06-29 00:00:00', 17),
(67, 67, 2, '', 'Donación', '2021-06-29 00:00:00', 16),
(68, 68, 2, '', 'Donación', '2021-06-29 00:00:00', 11),
(69, 69, 2, '', 'Donación', '2021-06-29 00:00:00', 21),
(70, 70, 2, '', 'Donación', '2021-06-29 00:00:00', 7),
(71, 71, 2, '', 'Donación', '2021-06-29 00:00:00', 33),
(72, 72, 2, '', 'Donación', '2021-06-29 00:00:00', 4),
(73, 73, 2, '', 'Donación', '2021-06-29 00:00:00', 56),
(74, 74, 2, '', 'Donación', '2021-06-29 00:00:00', 64),
(75, 75, 2, '', 'Donación', '2021-06-29 00:00:00', 61),
(76, 76, 2, '', 'Donación', '2021-06-29 00:00:00', 57),
(77, 77, 2, '', 'Donación', '2021-06-29 00:00:00', 58),
(79, 78, 2, '', 'Donación', '2021-06-30 00:00:00', 17),
(80, 79, 2, '', 'Donación', '2021-06-30 00:00:00', 129),
(81, 80, 2, '', 'Donación', '2021-06-30 00:00:00', 26),
(82, 81, 2, '', 'Donación', '2021-06-30 00:00:00', 265),
(83, 82, 2, '', 'Donación', '2021-06-30 00:00:00', 5),
(84, 83, 2, '', 'Donación', '2021-06-30 00:00:00', 38),
(85, 84, 2, '', 'Donación', '2021-06-30 00:00:00', 85),
(86, 85, 2, '', 'Donación', '2021-06-30 00:00:00', 65),
(87, 86, 2, '', 'Donación', '2021-06-30 00:00:00', 433),
(88, 87, 2, '', 'Donación', '2021-06-30 00:00:00', 385),
(89, 88, 2, '', 'Donación', '2021-06-30 00:00:00', 427),
(90, 89, 2, '', 'Donación', '2021-06-30 00:00:00', 13),
(91, 90, 2, '', 'Donación', '2021-06-30 00:00:00', 26),
(92, 91, 2, '', 'Donación', '2021-06-30 00:00:00', 15),
(93, 92, 2, '', 'Donación', '2021-06-30 00:00:00', 129),
(94, 93, 2, '', 'Donación', '2021-06-30 00:00:00', 44),
(95, 94, 2, '', 'Donación', '2021-06-30 00:00:00', 170),
(96, 95, 2, '', 'Donación', '2021-06-30 00:00:00', 124),
(97, 96, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(98, 97, 2, '', 'Donación', '2021-06-30 00:00:00', 40),
(99, 98, 2, '', 'Donación', '2021-06-30 00:00:00', 85),
(100, 99, 2, '', 'Donación', '2021-06-30 00:00:00', 85),
(101, 100, 2, '', 'Donación', '2021-06-30 00:00:00', 85),
(102, 101, 2, '', 'Donación', '2021-06-30 00:00:00', 85),
(103, 102, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(104, 103, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(105, 104, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(106, 105, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(107, 106, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(108, 107, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(109, 108, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(110, 109, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(111, 110, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(112, 111, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(113, 112, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(114, 113, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(115, 114, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(116, 115, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(117, 116, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(118, 117, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(119, 118, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(120, 119, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(121, 120, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(122, 121, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(123, 122, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(124, 123, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(125, 124, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(126, 125, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(127, 126, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(128, 127, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(129, 128, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(130, 129, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(131, 130, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(132, 131, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(133, 132, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(134, 133, 2, '', 'Donación', '2021-06-30 00:00:00', 113),
(135, 134, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(136, 135, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(137, 136, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(138, 137, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(139, 138, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(140, 139, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(141, 140, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(142, 141, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(143, 142, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(144, 143, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(145, 144, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(146, 145, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(147, 146, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(148, 147, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(149, 148, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(150, 149, 2, '', 'Donación', '2021-06-30 00:00:00', 90),
(151, 150, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(152, 151, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(153, 152, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(154, 153, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(155, 154, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(156, 155, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(157, 156, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(158, 157, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(159, 158, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(160, 159, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(161, 160, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(162, 161, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(163, 162, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(164, 163, 2, '', 'Donación', '2021-06-30 00:00:00', 121),
(165, 164, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(166, 165, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(167, 166, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(168, 167, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(169, 168, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(170, 169, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(171, 170, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(172, 171, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(173, 172, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(174, 173, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(175, 174, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(176, 175, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(177, 176, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(178, 177, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(179, 178, 2, '', 'Donación', '2021-06-30 00:00:00', 74),
(180, 179, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(181, 180, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(182, 181, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(183, 182, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(184, 183, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(185, 184, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(186, 185, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(187, 186, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(188, 187, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(189, 188, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(190, 189, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(191, 190, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(192, 191, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(193, 192, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(194, 193, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(195, 194, 2, '', 'Donación', '2021-06-30 00:00:00', 104),
(196, 195, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(197, 196, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(198, 197, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(199, 198, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(200, 199, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(201, 200, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(202, 201, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(203, 202, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(204, 203, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(205, 204, 2, '', 'Donación', '2021-06-30 00:00:00', 82),
(206, 205, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(207, 206, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(208, 207, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(209, 208, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(210, 209, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(211, 210, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(212, 211, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(213, 212, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(214, 213, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(215, 214, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(216, 215, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(217, 216, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(218, 217, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(219, 218, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(220, 219, 2, '', 'Donación', '2021-06-30 00:00:00', 103),
(221, 220, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(222, 221, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(223, 222, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(224, 223, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(225, 224, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(226, 225, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(227, 226, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(228, 227, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(229, 228, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(230, 229, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(231, 230, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(232, 231, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(233, 232, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(234, 233, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(235, 234, 2, '', 'Donación', '2021-06-30 00:00:00', 126),
(236, 235, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(237, 236, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(238, 237, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(239, 238, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(240, 239, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(241, 240, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(242, 241, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(243, 242, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(244, 243, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(245, 244, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(246, 245, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(247, 246, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(248, 247, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(249, 248, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(250, 249, 2, '', 'Donación', '2021-06-30 00:00:00', 105),
(251, 250, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(252, 251, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(253, 252, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(254, 253, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(255, 254, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(256, 255, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(257, 256, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(258, 257, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(259, 258, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(260, 259, 2, '', 'Donación', '2021-06-30 00:00:00', 56),
(261, 260, 2, '', 'Donación', '2021-06-30 00:00:00', 83),
(262, 261, 2, '', 'Donación', '2021-06-30 00:00:00', 51),
(263, 262, 2, '', 'Donación', '2021-06-30 00:00:00', 53),
(264, 263, 2, '', 'Donación', '2021-06-30 00:00:00', 58),
(265, 264, 2, '', 'Donación', '2021-06-30 00:00:00', 433),
(266, 265, 2, '', 'Donación', '2021-06-30 00:00:00', 1184),
(267, 266, 2, '', 'Donación', '2021-06-30 00:00:00', 1373),
(268, 267, 2, '', 'Donación', '2021-06-30 00:00:00', 50),
(269, 268, 2, '', 'Donación', '2021-06-30 00:00:00', 400),
(270, 269, 2, '', 'Donación', '2021-06-30 00:00:00', 114129),
(271, 270, 2, '', 'Compra', '2021-06-30 00:00:00', 114129),
(272, 271, 2, '', 'Compra', '2021-06-30 00:00:00', 111512),
(273, 272, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 19:59:19', 1020),
(274, 273, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:01:54', 1100),
(275, 274, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:03:08', 868),
(276, 275, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:04:43', 1080),
(277, 276, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:06:54', 84),
(278, 277, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:08:45', 700),
(279, 278, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:10:25', 624),
(280, 279, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:11:42', 100),
(281, 280, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:13:25', 112),
(282, 281, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:16:58', 20),
(283, 282, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:17:31', 5),
(284, 283, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:19:36', 5),
(285, 284, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:20:11', 60),
(286, 285, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:20:46', 50),
(287, 286, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:21:32', 30),
(288, 287, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:22:42', 50),
(289, 288, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:23:36', 200),
(290, 289, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:24:03', 98),
(291, 290, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:24:35', 50),
(292, 291, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:25:06', 50),
(293, 292, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:25:41', 45),
(294, 293, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:26:15', 31),
(295, 294, 3, 'Jesus Oropeza', 'Donación', '2021-08-17 20:26:37', 51),
(296, 295, 1, '', 'Donación', '2021-09-24 19:30:05', 43);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Libro`
--

CREATE TABLE `Libro` (
  `idLibro` int(11) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `Titulo` varchar(300) CHARACTER SET utf8mb4 NOT NULL,
  `Autor` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Ilustrador` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Editorial` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Coleccion` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Año` year(4) NOT NULL,
  `Paginas` int(11) DEFAULT NULL,
  `Formato` varchar(30) CHARACTER SET utf8mb4 DEFAULT NULL,
  `FechaIngreso` datetime NOT NULL DEFAULT current_timestamp(),
  `Ejemplares` int(11) NOT NULL,
  `Adquisicion` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Recibe` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Portada` varchar(300) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Estatus` int(11) NOT NULL DEFAULT 1,
  `idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Libro`
--

INSERT INTO `Libro` (`idLibro`, `idProveedor`, `Titulo`, `Autor`, `Ilustrador`, `Editorial`, `Coleccion`, `Año`, `Paginas`, `Formato`, `FechaIngreso`, `Ejemplares`, `Adquisicion`, `Recibe`, `Portada`, `Estatus`, `idUsuario`) VALUES
(1, 3, '09 Reforma política', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 99, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABReformaPolitica.JPG', 1, 2),
(2, 3, '08 Cultura y arte ', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 161, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABCulturaArte.JPG', 1, 2),
(3, 3, '07 Gestión de riesgos', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 98, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABGestionRiesgos.JPG', 1, 2),
(4, 3, '06 Agua', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 97, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABAgua.JPG', 1, 2),
(5, 3, '05 Salud', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 134, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABSalud.JPG', 1, 2),
(6, 3, '04 Educación básica y educación media superior', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 95, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABEducacionBasicaMS.JPG', 1, 2),
(7, 3, '03 Seguridad ciudadana', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 89, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABSeguridadCiudadana.JPG', 1, 2),
(8, 3, '02 Desarrollo economico y finanzas', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 129, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABDesarrolloEconomico.JPG', 1, 2),
(9, 3, '01 Política social', '', '', 'Miguel Angel Porrua', 'Los retos del distrito federal', 2012, 137, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABPoliticaSocial.JPG', 1, 2),
(10, 3, '14 Transparencia y rendicion de cuentas', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 174, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABTransparenciaRendicion.JPG', 1, 2),
(11, 3, '13 Evaluación de políticas públicas', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 252, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABEvaluacionPoliticasP.JPG', 1, 2),
(12, 3, '12 Gestion de crisis', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 157, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABGestionCrisis.JPG', 1, 2),
(13, 3, '11 Servicio profecional de carrera', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 213, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABServicioProfecional.JPG', 1, 2),
(14, 3, '10 Nueva gestión pública', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 251, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABNuevaGestionPublica.JPG', 1, 2),
(15, 3, '09 Ética pública', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 187, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABEticaPublica.JPG', 1, 2),
(16, 3, '08 Gestión estratégica', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 163, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABGestionEstrategica.JPG', 1, 2),
(17, 3, '07 Gestión de calidad', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 219, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABGestionCalidad.JPG', 1, 2),
(18, 3, '06 Administración pública del siglo XIX', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 204, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABAdminPublicaSXIX.JPG', 1, 2),
(19, 3, '05 Origanización e instituciones', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 245, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABOrganizacionInstituciones.JPG', 1, 2),
(20, 3, '04 Participación ciudadana en las políticas publicas', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 276, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABParticipacionCiudadana.JPG', 1, 2),
(21, 3, '03 Administración pública mexicana del siglo XX', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 181, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABAdminPublicaSXX.JPG', 1, 2),
(22, 3, '02 Administración pública', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 220, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABAdminPublica.JPG', 1, 2),
(23, 3, '01 Política pública', '', '', 'Siglo XXI', 'Biblioteca Basica de Administración Pública', 2010, 175, '', '2021-06-29 00:00:00', 6, 'Donación', '', 'DABPoliticaPublica.JPG', 1, 2),
(24, 3, 'Impartición de justicia jueces y política', '', '', 'Escuela de administración del Distrito Federal', '', 2015, 305, '', '2021-06-29 00:00:00', 27, 'Donación', '', 'DABImparticionJusticia.JPG', 1, 2),
(25, 3, 'La Danza de Nuestros Vecinos', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2012, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABDanzaNuetrosVecinos.JPG', 1, 2),
(26, 3, '¡Quiero Ser Astronauta!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABQuieroSerAstronauta.JPG', 1, 2),
(27, 3, '¡A Tejer Se Ha Dicho!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABATejerSeHaDicho.JPG', 1, 2),
(28, 3, 'Viajes a Través de tu Cuerpo', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABViajeCuerpo.JPG', 1, 2),
(29, 3, '¡A Volar Se Ha dicho!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABAVolarSeHaDicho.JPG', 1, 2),
(30, 3, 'La Maravillosa Vida Submarina', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABMaravillosaVidaSubmarina.JPG', 1, 2),
(31, 3, '¡Un Increible Espectáculo!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABIncreibleEspectaculo.JPG', 1, 2),
(32, 3, '¡Abraza la Tierra!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 44, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABAbrazaTierra.JPG', 1, 2),
(33, 3, '¡Chispas y Más chispas!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABChispasyMasChispas.JPG', 1, 2),
(34, 3, 'Las Naves Espaciales', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABNavesEspaciales.JPG', 1, 2),
(35, 3, '¡Calentando motores!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABCalentandoMotores.JPG', 1, 2),
(36, 3, 'Ciegos Ilustres en la Historia', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABCiegosIlustres.JPG', 1, 2),
(37, 3, '¡Qué Dijiste!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABQueDijiste.JPG', 1, 2),
(38, 3, '¡A Toda Máquina!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABATodaMaquina.JPG', 1, 2),
(39, 3, '¡Todo Marcha Sobre Ruedas!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 44, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABTodoMarchaRuedas.JPG', 1, 2),
(40, 3, '¡A pensar, Sentir y… Respirar!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABPensarSentirRespirar.JPG', 1, 2),
(41, 3, 'Cuida lo que Comes, Cuida tu Salud', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABCuidaLoQueComes.JPG', 1, 2),
(42, 3, '¡Todos a Bordo!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABTodosAbordo.JPG', 1, 2),
(43, 3, 'De Cara al Futuro', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABDeCaraAlFuturo.JPG', 1, 2),
(44, 3, '¿Una Locomotora es una Loca con Motor?', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABLocomotoraLoca.JPG', 1, 2),
(45, 3, 'El Acompañante Silencioso', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABAcompañamientoSilencioso.JPG', 1, 2),
(46, 3, '¿Detectives o Espias?', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABDetectivesEspias.JPG', 1, 2),
(47, 3, '¿De Dónde Salieron Esos Bichos?', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABDondeSalieronBichos.JPG', 1, 2),
(48, 3, '¡No Inventes!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABNoInventesInventos.JPG', 1, 2),
(49, 3, '¡Ah… Si Tuvieramos Alas!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABAhSiTuvieramosAlas.JPG', 1, 2),
(50, 3, 'Mil Tonos de Verde', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABMilTonosVerde.JPG', 1, 2),
(51, 3, '¡Estrellas de Colores! ¡Como las de la Maestra!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 32, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABEstrellasColores.JPG', 1, 2),
(52, 3, 'Los Reptiles No son Monstruos', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABReptilesNoSonMonstruos.JPG', 1, 2),
(53, 3, 'El Clima ', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'DABElClima.JPG', 1, 2),
(54, 3, '¡Criaturas Increibles!', 'Laura Patricia Caballero Leal', 'Hilda M. Caballero Leal ', 'Constantine Editores', 'La Pandilla del Conocimiento en Braille y en Tinta', 2017, 48, 'Braille', '2021-06-29 00:00:00', 42, 'Donación', '', 'img_portada.png', 1, 2),
(55, 3, 'Juanito y el Frijol Mágico', 'Pilar Tapia', 'Gill Guile', 'Brimax Books', '', 1995, 12, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABJuanitoFrijoM.JPG', 1, 2),
(56, 3, 'Los Muertos Andan en Bici', 'Christel Guezka', 'Betenia Zacarías', 'El Naranjo', '', 2012, 37, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABMuertosAndanBici.JPG', 1, 2),
(57, 3, 'Diario de NIKKI: Crónicas de Una Vida Muy Poco Glamorosa', 'Rachel Renée Rusell', 'Rachel Renée Rusell', 'Océano de Mexico', '', 2015, 282, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABDiarioNikki.JPG', 1, 2),
(58, 3, 'Diario de NIKKI 3: Una Estrella del Pop Muy Poco Brillante ', 'Rachel Renée Rusell', 'Rachel Renée Rusell', 'Océano de Mexico', '', 2016, 311, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABDiarioNikki3.JPG', 1, 2),
(59, 3, 'Bodas de Sangre', 'Federico García Lorca', 'Shutterstock', 'Planeta Mexicana', '', 2015, 182, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABBodasSangre.JPG', 1, 2),
(60, 3, 'Salvemos a Nuetros Monstruos', 'Gabriel Leveroni', 'Juan Gedovius', 'Santillana Ediciones Generales', '', 2011, 25, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABSalvemosMonstruos.JPG', 1, 2),
(61, 3, 'El Libro de los Duendes', 'Gabriela de los Ángeles Santana', 'Alberto Flanders', 'Selector', '', 1997, 233, '', '2021-06-29 00:00:00', 1, 'Donación', '', 'DABLibroDuendes.JPG', 1, 2),
(62, 3, 'Encontre un…', 'Maria Baranda', 'Cecilia Varela', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 2, 'Donación', '', 'DABEncontreUn.JPG', 1, 2),
(63, 3, 'Lino Felino', 'Esteli Meza', '', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 16, 'Donación', '', 'DABLinoFelino.JPG', 1, 2),
(64, 3, 'Dragones de Vacaciones ', 'Emilio Lone ', 'Rodrigo Vargas', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 12, 'Donación', '', 'DABDragonesVacaciones.JPG', 1, 2),
(65, 3, 'Foto Sorpresa', 'Elena Dreser', 'Flavia Zorrilla', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 9, 'Donación', '', 'DABFotoSorpresa.JPG', 1, 2),
(66, 3, 'Gumaro', 'Emilio Lone ', 'Cecilia Rebora', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 17, 'Donación', '', 'DABGumaro.JPG', 1, 2),
(67, 3, 'Marina la Furiosa', 'Jaime Alfonso Sandoval', 'Marcos Almada', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 16, 'Donación', '', 'DABMarianaFuriosa.JPG', 1, 2),
(68, 3, 'Wuaaaaaa!!!', 'Judy Goldman', 'Evelyn Alarcón', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 11, 'Donación', '', 'DABWuaaaaa.JPG', 1, 2),
(69, 3, 'Andrea se Viste de Rojo', 'Norma Muñoz Ledo', 'Víctor García Bernal', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 21, 'Donación', '', 'DABAndreaRojo.JPG', 1, 2),
(70, 3, 'El Mejor Regalo', 'Christel Guezka', 'Jaqueline Velázquez', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 7, 'Donación', '', 'DABMejorRegalo.JPG', 1, 2),
(71, 3, 'Ojitos de Golondrina', 'Elena Dreser', 'Claudia Navarro', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 33, 'Donación', '', 'DABOjitosGolondrina.JPG', 1, 2),
(72, 3, 'Abracadabra', 'Luz Chapela', 'Rodrigo Vargas', '3 abejas', 'Cuéntamelo otra vez', 2014, 18, '', '2021-06-29 00:00:00', 4, 'Donación', '', 'DABAbracadabra.JPG', 1, 2),
(73, 3, 'El Principito', 'Antoine de Saint-Exupéry', '', 'Unidos por la Lectura', '', 2014, 87, 'Audiolibro', '2021-06-29 00:00:00', 56, 'Donación', '', 'DABPrincipito.JPG', 1, 2),
(74, 3, 'Cuento de Navidad', 'Charles Dickens', '', 'Unidos por la Lectura', '', 2014, 64, 'Audiolibro', '2021-06-29 00:00:00', 64, 'Donación', '', 'DABCuentoNovedad.JPG', 1, 2),
(75, 3, 'El Fantasma de Canterville', 'Oscar Wilde', '', 'Unidos por la Lectura', '', 2014, 155, 'Audiolibro', '2021-06-29 00:00:00', 61, 'Donación', '', 'DABFantasmaCanterville.JPG', 1, 2),
(76, 3, 'Cuentos Clásicos Tomo 1', '', '', 'Unidos por la Lectura', '', 2014, 143, 'Audiolibro', '2021-06-29 00:00:00', 57, 'Donación', '', 'DABCuentosClasicoI.JPG', 1, 2),
(77, 3, 'Cuentos Clásicos Tomo 2', '', '', 'Unidos por la Lectura', '', 2014, 140, 'Audiolibro', '2021-06-29 00:00:00', 58, 'Donación', '', 'DABCuentosClasicosII.JPG', 1, 2),
(78, 3, 'SaludArte ', '', '', 'Gobierno de la Ciudad de México', '', 2013, 87, '', '2021-06-30 00:00:00', 17, 'Donación', '', 'DABSaludArte.JPG', 1, 2),
(79, 3, 'Taller: El Cuidado y la Reparación de los Libros', 'Mireya Hernández Ibarra', '', 'Consejo Nacional para la Cultura y las Artes', '', 2014, 44, '', '2021-06-30 00:00:00', 129, 'Donación', '', 'DABTallerReparacionLibros.JPG', 1, 2),
(80, 3, '¡Educación para la Salud!', '', 'Tania Juárez Ceciliano', 'Gobierno de la Ciudad de México', '', 2011, 83, '', '2021-06-30 00:00:00', 26, 'Donación', '', 'DABEducacionSalud.JPG', 1, 2),
(81, 3, 'Constitución Mexicana para Niños y Jóvenes de Edad y Espíritu', 'Jaime Miguel Moreno Garavilla ', '', 'Gobierno de la Ciudad de México', '', 2015, 277, '', '2021-06-30 00:00:00', 265, 'Donación', '', 'DABConstitucionMexicana.JPG', 1, 2),
(82, 3, 'Otras Voces y Otros Ecos del 68', 'Salvador Martínez Della Roca', '', 'Gobierno de la Ciudad de México', '', 2013, 437, '', '2021-06-30 00:00:00', 5, 'Donación', '', 'DABVocesEcos682.JPG', 1, 2),
(83, 3, 'Cultura y Literatura', 'Rafael Pérez Gay', '', 'Cal y Arena', '', 2014, 263, '', '2021-06-30 00:00:00', 38, 'Donación', '', 'DABCulturaLiteratura.JPG', 1, 2),
(84, 3, 'Culture and Literature', 'Rafael Pérez Gay', '', 'Cal y Arena', '', 2014, 263, '', '2021-06-30 00:00:00', 85, 'Donación', '', 'DABCultureLiterature.JPG', 1, 2),
(85, 3, 'Metrópolis', '', '', 'Gobierno de la Ciudad de México', '', 2010, 306, '', '2021-06-30 00:00:00', 65, 'Donación', '', 'DABMetropolis.JPG', 1, 2),
(86, 3, 'Comic 3: La Conquista', 'Bernardo García Martínez', 'Ricardo Pelaez', 'Cámara de Diputados', '', 2013, 59, '', '2021-06-30 00:00:00', 433, 'Donación', '', 'DABConquista.JPG', 1, 2),
(87, 3, 'Comic 2: Las Reformas Borbónicas', 'Luis Jauregui', 'Héctor Dávila', 'Cámara de Diputados', '', 2013, 59, '', '2021-06-30 00:00:00', 385, 'Donación', '', 'DABReformasBorbonicas.JPG', 1, 2),
(88, 3, 'Comic 1: Del Imperio al Triunfo de la Reforma', 'Josefina Zoraida Vázquez', 'Sergio Vicencio', 'Cámara de Diputados', '', 2013, 59, '', '2021-06-30 00:00:00', 427, 'Donación', '', 'DABImperioTriunfoReforma.JPG', 1, 2),
(89, 3, 'Volumen 13. Euskal Etxea de la Ciudad de México', 'Amaya Garritz Riuz', '', 'Servicio de Publicaciones del Gobierno Vasco', '', 2003, 233, '', '2021-06-30 00:00:00', 13, 'Donación', '', 'DABUrazandi.JPG', 1, 2),
(90, 3, 'Derechos de las Niñas y los Niños Mexicanos', '', 'José L. Martínez Rodríguez', 'Agencia Promotora de Publicaciones', '', 2017, 167, '', '2021-06-30 00:00:00', 26, 'Donación', '', 'DABDerechosNiños.JPG', 1, 2),
(91, 3, 'Grandes Ciudades del Mundo', '', '', 'Sol 90 ', '', 2017, 255, '', '2021-06-30 00:00:00', 15, 'Donación', '', 'DABGrandesCiudadesMundo.JPG', 1, 2),
(92, 3, 'Haciendo facil lo dificil', 'David Sola', '', 'Agencia Promotora de Publicaciones', '', 2017, 128, '', '2021-06-30 00:00:00', 129, 'Donación', '', 'DABHaciendoFacilDificil.JPG', 1, 2),
(93, 3, 'Niños y Niñas por un México sin Acoso Escolar', '', 'José L. Martínez Rodríguez', 'Agencia Promotora de Publicaciones', '', 2017, 150, '', '2021-06-30 00:00:00', 44, 'Donación', '', 'DABAcosoEscolar.JPG', 1, 2),
(94, 3, 'República Mexicana', '', 'José L. Martínez Rodríguez', 'Agencia Promotora de Publicaciones', '', 2017, 102, '', '2021-06-30 00:00:00', 170, 'Donación', '', 'DABRepublicaMexicana.JPG', 1, 2),
(95, 3, 'Mitos y Leyendas de México', '', 'Aranzazu Cruz Zumaran', 'Agencia Promotora de Publicaciones', '', 2017, 69, '', '2021-06-30 00:00:00', 119, 'Donación', '', 'DABMitosLeyendasMexico.JPG', 1, 2),
(96, 3, 'La Democracia', 'Diana Luz Oliva Cárdenas', 'Karina Landa', 'Agencia Promotora de Publicaciones', '', 2017, 95, '', '2021-06-30 00:00:00', 56, 'Donación', '', 'DABDemocracia.JPG', 1, 2),
(97, 3, 'Niñas y niños por un México sin obesidad', '', 'José L. Martínez Rodríguez', 'Agencia Promotora de Publicaciones', '', 2017, 130, '', '2021-06-30 00:00:00', 40, 'Donación', '', 'img_portada.png', 1, 2),
(98, 3, 'Bellas Artes', '', 'Benjamin Mendez', 'Agencia Promotora de Publicaciones', 'Cambiaron el Rumbo de la Historia', 2017, 135, '', '2021-06-30 00:00:00', 85, 'Donación', '', 'DABBellasArtes.JPG', 1, 2),
(99, 3, 'Ciencias', '', 'Benjamin Mendez', 'Agencia Promotora de Publicaciones', 'Cambiaron el Rumbo de la Historia', 2017, 135, '', '2021-06-30 00:00:00', 85, 'Donación', '', 'DABCiencias.JPG', 1, 2),
(100, 3, 'Literatura', '', 'Benjamin Mendez', 'Agencia Promotora de Publicaciones', 'Cambiaron el Rumbo de la Historia', 2017, 87, '', '2021-06-30 00:00:00', 85, 'Donación', '', 'DABLiteratura.JPG', 1, 2),
(101, 3, 'Sociedad', '', 'Benjamin Mendez', 'Agencia Promotora de Publicaciones', 'Cambiaron el Rumbo de la Historia', 2017, 183, '', '2021-06-30 00:00:00', 85, 'Donación', '', 'DABSociedad.JPG', 1, 2),
(102, 3, 'El Sistema Solar', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABSistemaSolar.JPG', 1, 2),
(103, 3, 'Mamíferos', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABMamiferos.JPG', 1, 2),
(104, 3, 'Reptiles', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABReptilesBE.JPG', 1, 2),
(105, 3, 'Historia de la Ciencia', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABHistoriaCiencia.JPG', 1, 2),
(106, 3, 'Animales Prehistóricos', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABAnimalesPrehistoricos.JPG', 1, 2),
(107, 3, 'Animales Marinos', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABAnimalesMarinos.JPG', 1, 2),
(108, 3, 'Montañas y Bosques', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABMontañasBosques.JPG', 1, 2),
(109, 3, 'El Origen de la Vida', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABOrigenVida.JPG', 1, 2),
(110, 3, 'La Revolución Industrial ', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABRevolucionIndustrial.JPG', 1, 2),
(111, 3, 'Minas y Minerales', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABMinasMinerales.JPG', 1, 2),
(112, 3, 'Grandes Exploradores ', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'img_portada.png', 1, 2),
(113, 3, 'Fuentes de Energía', '', '', 'Sol 90 ', 'Biblioteca del Estudiante', 2017, 31, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABFuentesEnergia.JPG', 1, 2),
(114, 3, 'Invertebrados II', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABInvertebradosII.JPG', 1, 2),
(115, 3, 'Invertebrados I', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABInvertebradrosI.JPG', 1, 2),
(116, 3, 'Peces IV', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABPecesIV.JPG', 1, 2),
(117, 3, 'Peces III', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABPecesIII.JPG', 1, 2),
(118, 3, 'Peces II', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABPecesII.JPG', 1, 2),
(119, 3, 'Peces I', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABPecesI.JPG', 1, 2),
(120, 3, 'Anfibios ', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAnfibios.JPG', 1, 2),
(121, 3, 'Reptiles II', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABReptilesII.JPG', 1, 2),
(122, 3, 'Reptiles I', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABReptilesI.JPG', 1, 2),
(123, 3, 'Aves V', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAvesV.JPG', 1, 2),
(124, 3, 'Aves IV', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAvesIV.JPG', 1, 2),
(125, 3, 'Aves III', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAvesIII.JPG', 1, 2),
(126, 3, 'Aves II', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAvesII.JPG', 1, 2),
(127, 3, 'Aves I', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABAvesI.JPG', 1, 2),
(128, 3, 'Mamíferos V', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABMamiferosV.JPG', 1, 2),
(129, 3, 'Mamíferos IV', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABMamiferosIV.JPG', 1, 2),
(130, 3, 'Mamíferos III', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABMamiferosIII.JPG', 1, 2),
(131, 3, 'Mamíferos II', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABMamiferosII.JPG', 1, 2),
(132, 3, 'Mamíferos I', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABMamiferosI.JPG', 1, 2),
(133, 3, 'Reino Animal', '', 'Leonardo Cesar', 'Sol 90 ', 'Enciclopedia de los Animales', 2017, 79, '', '2021-06-30 00:00:00', 108, 'Donación', '', 'DABReinoAnimal.JPG', 1, 2),
(134, 3, 'Egipto y las Primeras Civilizaciones EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABEgiptoPrimerasCiv.JPG', 1, 2),
(135, 3, 'Grecia y Roma EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABGreciaRoma.JPG', 1, 2),
(136, 3, 'Peces y Anfibios EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABPecesAnfibiosEI.JPG', 1, 2),
(137, 3, 'Ciencia EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABCienciaEI.JPG', 1, 2),
(138, 3, 'Aves', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABAvesEI.JPG', 1, 2),
(139, 3, 'Mamíferos EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABLosMamiferosEI.JPG', 1, 2),
(140, 3, 'Historia Contemporánea', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABHistoriaContemporaneaEI.JPG', 1, 2),
(141, 3, 'Arte, musica y Literatura', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABArteMusicaLiteratura.JPG', 1, 2),
(142, 3, 'Reptiles EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABReptiles.JPG', 1, 2),
(143, 3, 'insectos e Invertebrados EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABInsectosInvertebradosEI.JPG', 1, 2),
(144, 3, 'Reino Vegetal', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABReinoVegetal.JPG', 1, 2),
(145, 3, 'Grandes maestros de la pintura', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABGrandesMaestrosPintura.JPG', 1, 2),
(146, 3, 'Planeta Tierra EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABPlanetaTierraEI.JPG', 1, 2),
(147, 3, 'Evolución y genetica', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABEvolucionGenetica2.JPG', 1, 2),
(148, 3, 'La alimentación EI ', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABAlimentacion.JPG', 1, 2),
(149, 3, 'Medio Ambiente EI', '', '', 'Sol 90 ', 'Enciclopedia Infantil', 2017, 63, '', '2021-06-30 00:00:00', 89, 'Donación', '', 'DABMedioAmbienteEI.JPG', 1, 2),
(150, 3, 'La Tumba de Tutankamon ', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABTumbaTutankamon.JPG', 1, 2),
(151, 3, 'El Ocaso de los Mayas', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABOcasoMayas.JPG', 1, 2),
(152, 3, 'Las Ciudades Perdidas del Amazonas', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABCiudadePerdidasAmazonas.JPG', 1, 2),
(153, 3, 'Atlántida. La Leyenda del Continente Perdido', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABAtlantidaContinentePer.JPG', 1, 2),
(154, 3, 'Los Últimos Dinosaurios', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABUltimosDinosaurios.JPG', 1, 2),
(155, 3, 'El Primer Ser Humano', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABPrimerSerHumano.JPG', 1, 2),
(156, 3, 'La Muerte de Hitler', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABMuerteHitler.JPG', 1, 2),
(157, 3, 'Las Ciudades de Machu Picchu', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABCiudadMachuPicchu.JPG', 1, 2),
(158, 3, 'Los Misterios de la Segunda Guerra Mundial', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'img_portada.png', 1, 2),
(159, 3, 'Los Moais de la Isla de Pascua', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'img_portada.png', 1, 2),
(160, 3, 'Las Pirámides de Egipto', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABPiramidesEgipto.JPG', 1, 2),
(161, 3, 'Los Caballeros Templarios', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABCaballerosTemplarios.JPG', 1, 2),
(162, 3, 'El Complejo de Stonehenge', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABComplejoStonehenge.JPG', 1, 2),
(163, 3, 'La Guerra de Troya', '', '', 'Sol 90 ', 'Enigmas de la Historia', 2017, 47, '', '2021-06-30 00:00:00', 121, 'Donación', '', 'DABGuerraTroya.JPG', 1, 2),
(164, 3, 'Nuevas Tecnologías y Educación', 'Andrés Puyol', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABNuevasTecnologias.JPG', 1, 2),
(165, 3, 'Bullying y Acoso Escolar', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABBillyingAcosoEs.JPG', 1, 2),
(166, 3, 'Técnicas de Disciplina', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABTecnicasDiciplina.JPG', 1, 2),
(167, 3, 'Cómo Tratar la Discapacidad Intelectual', 'Valentín Lacalle', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABDiscapacidadIntelectual.JPG', 1, 2),
(168, 3, 'Niños Desobedientes y Otros Problemas de Conducta', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABNiñosDesobedientes.JPG', 1, 2),
(169, 3, 'Cómo Corregir los Malos Modales', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABCorregirModales.JPG', 1, 2),
(170, 3, 'Fomentar la Responsabilidad', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABFomentarResponsabilidad.JPG', 1, 2),
(171, 3, 'Cómo Mejorar la Autoestima', 'Carolina Narbón de la Villa', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABMejorarAutoestima.JPG', 1, 2),
(172, 3, 'Superar el Divorcio de los Padres', 'Silvia Igualador Villar', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABSuperarDivorcioPadres.JPG', 1, 2),
(173, 3, 'Cómo Tratar los Celos', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABTratarCelos.JPG', 1, 2),
(174, 3, 'El Estrés Infantil', 'Silvia Igualador Villar', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABEstresInfantil.JPG', 1, 2),
(175, 3, 'El Problema de las Drogas', 'Jesús Linares', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABProblemasDrogas.JPG', 1, 2),
(176, 3, 'Niños y Abuelos', 'Mercedes Salvador', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABNiñosAbuelos.JPG', 1, 2),
(177, 3, 'Enseñar a estudiar, Enseñar a Aprender', 'Jesús Jarque García', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABEnseñarEstudiar.JPG', 1, 2),
(178, 3, 'Los Transtornos de la Alimentación', 'Tatiana Muñoz Gonzalez', '', 'Agencia Promotora de Publicaciones', 'Escuela para Padres', 2017, 50, '', '2021-06-30 00:00:00', 74, 'Donación', '', 'DABtranstornosAlimentacion.JPG', 1, 2),
(179, 3, 'Vikingos y Celtas', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABVikingosCeltas.JPG', 1, 2),
(180, 3, 'Imperio Bizantino', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABImperioBizantino.JPG', 1, 2),
(181, 3, 'Islam', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABIslam.JPG', 1, 2),
(182, 3, 'El Pueblo Hebreo', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABPuebloHebreo.JPG', 1, 2),
(183, 3, 'Persia', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABPersia.JPG', 1, 2),
(184, 3, 'India', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABIndia.JPG', 1, 2),
(185, 3, 'Imperio Carolingio', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABImperioCarolingio.JPG', 1, 2),
(186, 3, 'Japón Ancestral', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABJaponAncestral.JPG', 1, 2),
(187, 3, 'Antiguo Egipto', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABAntiguoEgipto.JPG', 1, 2),
(188, 3, 'Incas y Culturas Andinas', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABIncasCulturasAnd.JPG', 1, 2),
(189, 3, 'Roma Imperial', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABRomaImperial.JPG', 1, 2),
(190, 3, 'Grecia Clásica', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABGreciaClasica.JPG', 1, 2),
(191, 3, 'Imperio Azteca', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABImperioAzteca.JPG', 1, 2),
(192, 3, 'Mayas', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABMayas.JPG', 1, 2),
(193, 3, 'Antigua China', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABAntiguaChina.JPG', 1, 2),
(194, 3, 'Mesopotamia', '', 'Dante Ginevra', 'Sol 90 ', 'Grandes Civilizaciones', 2017, 95, '', '2021-06-30 00:00:00', 104, 'Donación', '', 'DABMesopotamia.JPG', 1, 2),
(195, 3, 'Miguel Hidalgo', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABMiguelHidalgo.JPG', 1, 2),
(196, 3, 'José María Morelos', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABJoseMariaMorelos.JPG', 1, 2),
(197, 3, 'Emiliano Zapata', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABEmilianoZapata.JPG', 1, 2),
(198, 3, 'Francisco I. Madero', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABFranciscoMadero.JPG', 1, 2),
(199, 3, 'Pancho Villa', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABPanchoVilla.JPG', 1, 2),
(200, 3, 'Porfirio Díaz', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABPorfirioDiaz.JPG', 1, 2),
(201, 3, 'Benito Juárez', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABBenitoJuarez.JPG', 1, 2),
(202, 3, 'lazaro Cárdenas', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABLazaroCardenas.JPG', 1, 2),
(203, 3, 'Sor Juana Inés de la Cruz', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABSorJuanaInes.JPG', 1, 2),
(204, 3, 'Alfonso Reyes', '', 'José Luis Martínez', 'Agencia Promotora de Publicaciones', 'Grandes Mexicanos para Niños', 2017, 43, '', '2021-06-30 00:00:00', 82, 'Donación', '', 'DABAlfonsoReyes.JPG', 1, 2),
(205, 3, 'Las Habichuelas Mágicas', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABHabichuelasMagicas.JPG', 1, 2),
(206, 3, 'Aladino y la Lámpara Maravillosa', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABAladinoLampara.JPG', 1, 2),
(207, 3, 'Peter Pan', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABPeterPan.JPG', 1, 2),
(208, 3, 'Alicia en el Pais de las Maravillas', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABAliciaPaisMaravillas.JPG', 1, 2),
(209, 3, 'Moby Dick', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABMobyDick.JPG', 1, 2),
(210, 3, 'Blancanieves', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABBlancanieves.JPG', 1, 2),
(211, 3, 'La Bella y la Bestia', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABBellaBestia.JPG', 1, 2),
(212, 3, 'Las Aventuras de Tom Sawyer', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABAventurasTomSawyer.JPG', 1, 2),
(213, 3, 'Don Quijote de la Mancha', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABQuijoteMancha.JPG', 1, 2),
(214, 3, 'El Flautista de Hamelin', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABFlautistaHamelin.JPG', 1, 2),
(215, 3, 'Los Viajes de Gulliver', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABViajesGulliver.JPG', 1, 2),
(216, 3, 'Ali Baba y los 40 Ladrones', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABAlibaba40.JPG', 1, 2),
(217, 3, 'Pinocho', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABPinocho2.JPG', 1, 2),
(218, 3, 'El Libro de la Selva', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABLibroSelva2.JPG', 1, 2),
(219, 3, 'Hansel y Gretel', '', 'Sebastian Giacobino', 'Sol 90 ', 'Los Mejores Cuentos Bilingües', 2017, 47, '', '2021-06-30 00:00:00', 102, 'Donación', '', 'DABHanselGretel.JPG', 1, 2),
(220, 3, 'El Mundo en Cifras', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABMundoCifras.JPG', 1, 2),
(221, 3, 'Oceanía', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABOceania.JPG', 1, 2),
(222, 3, 'Asia III', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAsiaIII.JPG', 1, 2),
(223, 3, 'Asia II', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAsiaII.JPG', 1, 2),
(224, 3, 'Asia I', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAsiaI.JPG', 1, 2),
(225, 3, 'Africa II', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAfricaII.JPG', 1, 2),
(226, 3, 'Africa I', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAfricaI.JPG', 1, 2),
(227, 3, 'Europa IV', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABEuropaIV.JPG', 1, 2),
(228, 3, 'Europa III', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABEuropaIII.JPG', 1, 2),
(229, 3, 'Europa II', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABEuropaII.JPG', 1, 2),
(230, 3, 'Europa I', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABEuropaI.JPG', 1, 2),
(231, 3, 'América del Sur', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAmericaSur.JPG', 1, 2),
(232, 3, 'América del Norte', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAmericaNorte.JPG', 1, 2),
(233, 3, 'América', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABAmerica.JPG', 1, 2),
(234, 3, 'El Mundo', '', '', 'Sol 90 ', 'Mi primer atlas del mundo', 2017, 50, '', '2021-06-30 00:00:00', 126, 'Donación', '', 'DABMundo.JPG', 1, 2),
(235, 3, 'Peces y Anfibios ', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABPecesAnfibios2.JPG', 1, 2),
(236, 3, 'Insectos e Invertebrados', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 63, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABInsectosInvertebrados.JPG', 1, 2),
(237, 3, 'El Reino Vegetal', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABReinoVegetal2.JPG', 1, 2),
(238, 3, 'El Planeta Tierra', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABPlanetaTierra.JPG', 1, 2),
(239, 3, 'Los Mamíferos', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABLosMamiferos.JPG', 1, 2),
(240, 3, 'Las Aves', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 50, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABLasAves2.JPG', 1, 2),
(241, 3, 'Los Reptiles', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 50, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABReptiles2.JPG', 1, 2),
(242, 3, 'Paraísos Naturales', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 50, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABParaisosNaturales.JPG', 1, 2),
(243, 3, 'Mares y Océanos', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABMareasOceanos.JPG', 1, 2),
(244, 3, 'Volcanes y Terremotos', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABVolcaneTerremotos.JPG', 1, 2),
(245, 3, 'Exploradores', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABExploradores.JPG', 1, 2),
(246, 3, 'Grandes Inventos', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABGrandesInventos.JPG', 1, 2),
(247, 3, 'Ríos y Lagos', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABRiosLagos.JPG', 1, 2),
(248, 3, 'Las Montañas y bosques', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABMontañasBosques2.JPG', 1, 2),
(249, 3, 'El Medio ambiente', '', '', 'Sol 90 ', 'Mi primera enciclopedia', 2017, 47, '', '2021-06-30 00:00:00', 105, 'Donación', '', 'DABMedioAmbientePE.JPG', 1, 2),
(250, 3, 'Colmillo Blanco', 'Jack London', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABColmilloBlanco.JPG', 1, 2),
(251, 3, 'El Hombre de la Máscara de Hierro', 'Alejandro Dumas', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABHombreMascaraHierro.JPG', 1, 2),
(252, 3, 'Libro de la Selva', 'Rudyard Kipling', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABLibroSelva.JPG', 1, 2),
(253, 3, 'El Retrato de Dorian Gray', 'Oscar Wilde', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABRetratoDorianGray.JPG', 1, 2),
(254, 3, 'La Isla del Tesoro', 'Robert Louis Stevenson', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABIslaTesoro.JPG', 1, 2),
(255, 3, 'La Máquina del Tiempo', 'Herbert George Wells', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABMaquinaTiempo.JPG', 1, 2),
(256, 3, 'Los Tres Mosqueteros', 'Alejandro Dumas', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABTresMosqueteros.JPG', 1, 2),
(257, 3, 'Las Aventuras de Sherlock Holmes', 'Arthur Conan Doyle', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABAventurasSherlockHolmes.JPG', 1, 2);
INSERT INTO `Libro` (`idLibro`, `idProveedor`, `Titulo`, `Autor`, `Ilustrador`, `Editorial`, `Coleccion`, `Año`, `Paginas`, `Formato`, `FechaIngreso`, `Ejemplares`, `Adquisicion`, `Recibe`, `Portada`, `Estatus`, `idUsuario`) VALUES
(258, 3, 'Viaje al Centro de la Tierra', 'Julio Verne', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABViajeCentroTierra.JPG', 1, 2),
(259, 3, 'La Leyenda de Robin Hood', 'Howard Pyle', 'Marcelo Sosa', 'Sol 90 ', 'Novelas Juveniles Bilingües', 2017, 63, '', '2021-06-30 00:00:00', 51, 'Donación', '', 'DABLeyendaRobinHood.JPG', 1, 2),
(260, 2, 'Sombras. Cuentos de Extraña Imaginación ', '', 'Abraham Balcazar', 'Ediciones Castillo', '', 2019, 156, '', '2021-06-30 00:00:00', 79, 'Donación', '', 'DABSombras.JPG', 1, 2),
(261, 2, 'Ella Trae la Lluvia', 'Martha Riva Palacio Obón', 'Roger  Ycaza', 'El Naranjo', '', 2019, 99, '', '2021-06-30 00:00:00', 50, 'Donación', '', 'DABEllaTraeLaLluvia.JPG', 1, 2),
(262, 2, 'La Guardia de las Lechuzas', 'Antonio Ramos Revillas', 'Isidro R. Esquivel', 'El Naranjo', '', 2019, 125, '', '2021-06-30 00:00:00', 52, 'Donación', '', 'DABGuaridaLechuzas.JPG', 1, 2),
(263, 2, 'Los Yanquis en México', 'Guillermo Prieto', '', 'Fondo de Cultura Económica', 'Vientos del pueblo', 2019, 63, '', '2021-06-30 00:00:00', 49, 'Donación', '', 'DABYanquisMexico.JPG', 1, 2),
(264, 2, 'Los Convidados de Agosto', 'Rosario Castellanos', 'Ricado Peláez', 'Fondo de Cultura Económica', 'Vientos del pueblo', 2019, 44, '', '2021-06-30 00:00:00', 372, 'Donación', '', 'DABConvidadosAgosto.JPG', 1, 2),
(265, 2, 'Loxicha: Los Ejercitos de la Noche', 'Fabrizio Mejía Madrid', 'Ricado Peláez', 'Fondo de Cultura Económica', 'Vientos del pueblo', 2019, 28, '', '2021-06-30 00:00:00', 1123, 'Donación', '', 'DABLoxicha.JPG', 1, 2),
(266, 2, 'Rikky Tikky Tavi', 'Rudyard Kipling', 'Cesar Silva Páramo', 'Fondo de Cultura Económica', 'Vientos del pueblo', 2019, 30, '', '2021-06-30 00:00:00', 1317, 'Donación', '', 'DABRikkiTikkiTavi.JPG', 1, 2),
(267, 2, 'El Cuentacuentos', 'Antonia Michaelis', '', 'Fondo de Cultura Económica', 'A traves del espejo', 2019, 365, '', '2021-06-30 00:00:00', 36, 'Donación', '', 'DABCuentaCuentos.JPG', 1, 2),
(268, 2, 'Darwin y la Evolución por Selección Natural', 'Rosaura Riuz Gutiérrez', 'Sara Itzel López González', 'Universidad Nacional Autónoma de México', '', 2020, 87, '', '2021-06-30 00:00:00', 397, 'Donación', '', 'DABDarwinEvolucionNatural.JPG', 1, 2),
(269, 2, 'Humboldt. Ciudadano del Mundo', 'Jaime Labastida', 'Felipe de la Torre', 'Siglo XXI Editores', '', 2019, 39, '', '2021-06-30 00:00:00', 110411, 'Compra', '', 'DABHumboldt.JPG', 1, 2),
(270, 1, 'Las Aventuras de los Jóvenes Dioses ', 'Eduardo Galeano', 'Nivio López Vigil', 'Siglo XXI Editores', '', 2019, 38, '', '2021-06-30 00:00:00', 112975, 'Compra', '', 'DABAventurasDioses.JPG', 1, 2),
(271, 1, 'México de la A a la Z', 'Jaime Labastida', 'Carlos Mérida', 'Siglo XXI Editores', '', 2019, 62, '', '2021-06-30 00:00:00', 110423, 'Compra', '', 'DABMexicoAZ.JPG', 1, 2),
(272, 5, 'Vista parcial de la noche', 'Luiz Ruffato', '', 'Elephas', '', 2012, 0, '', '2021-08-17 19:59:19', 964, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.10.47.jpeg', 1, 3),
(273, 5, 'El libro de las imposibilidades.', 'Luiz Ruffato', '', 'Elephas', '', 2008, 0, '', '2021-08-17 20:01:54', 1044, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.11.30.jpeg', 1, 3),
(274, 5, 'Las madonnas de Echo Park', 'Brando Skyhorse', '', 'Elephas', '', 2012, 0, '', '2021-08-17 20:03:08', 857, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.13.10.jpeg', 1, 3),
(275, 5, 'Domingos sin Dios', 'Luiz Ruffato', '', 'Elephas', '', 2012, 0, '', '2021-08-17 20:04:43', 1076, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.12.08.jpeg', 1, 3),
(276, 5, 'El mundo enemigo', 'Luiz Ruffato', '', 'Elephas', '', 2005, 0, '', '2021-08-17 20:06:54', 83, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.08.57.jpeg', 1, 3),
(277, 5, 'Tren a Trieste', 'Domnica Radulescu', '', 'Elephas', '', 2008, 0, '', '2021-08-17 20:08:45', 689, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.13.54.jpeg', 1, 3),
(278, 5, 'Mamma, son tanto felice', 'Luiz Ruffato', '', 'Elephas', '', 2005, 0, '', '2021-08-17 20:10:25', 623, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.08.08.jpeg', 1, 3),
(279, 5, 'Choque de civilizaciones', 'Amara Lakhous', '', 'Elephas', '', 2007, 0, '', '2021-08-17 20:11:42', 99, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.10.20.jpeg', 1, 3),
(280, 5, 'El hijo eterno', 'Cristovao Tezza', '', 'Elephas', '', 2005, 0, '', '2021-08-17 20:13:25', 111, 'Donación', 'Jesus Oropeza', 'DABWhatsApp Image 2021-07-09 at 12.09.33.jpeg', 1, 3),
(281, 4, 'Neosubversión en la LIJ contemporánea: una aproximación a México y España', 'Laura Guerrero Guadarrama', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:16:58', 20, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(282, 4, 'Posmodernidad en la literatura infantil y juvenil', 'Laura Guerrero Guadarrama', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:17:31', 5, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(283, 4, 'Corporalidades', '', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:19:36', 5, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(284, 4, 'De cuerpos invisibles y placeres negados', 'María del Pilar Cruz Pérez', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:20:11', 60, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(285, 4, 'Desarrollo sostenible en cuatro pasos', 'Andrés Bucio Galindo', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:20:46', 50, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(286, 4, 'El problema del mal: un desafío para la persona humana', 'Andrés Navarro Zamora', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:21:32', 30, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(287, 4, 'Los derechos humanos en México: la tentación del autoritarismo', 'Jesús Acosta Ortíz', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:22:42', 50, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(288, 4, 'Construcciones de paz en México', 'Adela Salinas', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:23:36', 200, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(289, 4, 'El álgebra del misterio', 'Jorge F. Hernández', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:24:03', 98, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(290, 4, 'Del inicio al fin del amor: ruptura de pareja y salud mental', 'Miriam Wendolyn Barajas Márquez', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:24:35', 50, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(291, 4, 'En la mira: apuntes de un editor de fotografía', 'Ulises Castellanos', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:25:06', 50, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(292, 4, 'Mujeres en transición', '', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:25:41', 45, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(293, 4, 'La cultura y sus efectos sobre la psicología del mexicano', 'Rolando Díaz-Loving', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:26:15', 31, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(294, 4, 'Ayotzinapa, la otra historia', '', '', 'Universidad Iberoamericana', '', 2000, 0, '', '2021-08-17 20:26:37', 51, 'Donación', 'Jesus Oropeza', 'img_portada.png', 1, 3),
(295, 3, 'La trompetilla acustica', 'Leonora Carrington', '', 'Fondo de Cultura Económica', '', 2017, 176, '', '2021-09-24 19:30:05', 33, 'Donación', '', 'DABIMG_1406.JPG', 1, 1);

--
-- Disparadores `Libro`
--
DELIMITER $$
CREATE TRIGGER `entradasAI` AFTER INSERT ON `Libro` FOR EACH ROW BEGIN
    	INSERT INTO Entrada(idLibro,Recibe,idUsuario,Adquisicion,Cantidad)
        VALUES(new.idLibro,new.Recibe, new.idUsuario,new.Adquisicion,new.Ejemplares);
      END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Proveedor`
--

CREATE TABLE `Proveedor` (
  `idProveedor` int(11) NOT NULL,
  `Nombre` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Contacto` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Direccion` varchar(100) CHARACTER SET utf8mb4 NOT NULL,
  `Telefono` varchar(11) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Email` varchar(50) CHARACTER SET utf8mb4 DEFAULT NULL,
  `Estatus` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Proveedor`
--

INSERT INTO `Proveedor` (`idProveedor`, `Nombre`, `Contacto`, `Direccion`, `Telefono`, `Email`, `Estatus`) VALUES
(1, 'Siglo XXI', 'contacto Siglo XXI', 'Cerro de Agua 258 Romero de terreros Coyoacán CDMX', '5553392414', 'sigloxxi@gmail.com', 1),
(2, 'Fondo de Cultura Economica', 'Fondo de cultura economica', 'Picacho Ajusco 227 Parque Nacional Tlalpan CDMX', '5552274681', 'fondocultura@gmail.com', 1),
(3, 'Administración Pasada ', 'Administración Pasada ', 'Administración Pasada ', '0000000000 ', ' ', 1),
(4, 'Universidad Iberoamericana', 'Universidad Iberoamericana', '00', '', '', 1),
(5, 'Elephas', 'Lic.Josefina Isabel Fajardo Arias', 'Calle 2 113 Granjas San Antonio Iztapalapa CDMX', '5518281772', 'fita@editorialelephas.com', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Rol`
--

CREATE TABLE `Rol` (
  `idRol` int(11) NOT NULL,
  `rol` varchar(30) CHARACTER SET utf8mb4 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Rol`
--

INSERT INTO `Rol` (`idRol`, `rol`) VALUES
(1, 'Administrador'),
(2, 'Direccion'),
(3, 'Revisor'),
(4, 'Invitado'),
(5, 'visita');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Salida`
--

CREATE TABLE `Salida` (
  `idSalida` int(11) NOT NULL,
  `idDonatario` int(11) NOT NULL,
  `idUsuario` int(11) NOT NULL,
  `Fecha` datetime NOT NULL DEFAULT current_timestamp(),
  `Total` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Salida`
--

INSERT INTO `Salida` (`idSalida`, `idDonatario`, `idUsuario`, `Fecha`, `Total`) VALUES
(1, 1, 2, '2021-07-30 00:00:00', 70),
(2, 2, 2, '2021-04-06 00:00:00', 1630),
(3, 3, 3, '2021-07-13 22:51:51', 2100),
(4, 4, 3, '2021-08-17 18:54:17', 3),
(5, 5, 3, '2021-08-17 19:05:56', 1134),
(6, 6, 3, '2021-08-17 19:08:10', 6),
(7, 7, 3, '2021-08-17 19:21:16', 42),
(8, 8, 3, '2021-08-17 19:23:00', 6),
(9, 9, 3, '2021-08-30 22:46:52', 30),
(10, 10, 3, '2021-08-30 22:47:32', 1),
(11, 11, 3, '2021-08-30 22:48:05', 1),
(12, 12, 3, '2021-08-30 22:48:21', 1),
(13, 13, 3, '2021-08-30 22:50:00', 312),
(14, 14, 3, '2021-08-30 22:51:25', 9),
(15, 15, 3, '2021-09-23 01:20:27', 185),
(16, 16, 3, '2021-09-23 01:21:32', 50);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Usuario`
--

CREATE TABLE `Usuario` (
  `idUsuario` int(11) NOT NULL,
  `idRol` int(11) NOT NULL,
  `Nombre` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Apellido` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Email` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  `Clave` varchar(255) CHARACTER SET utf8mb4 NOT NULL,
  `Estatus` int(1) DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `Usuario`
--

INSERT INTO `Usuario` (`idUsuario`, `idRol`, `Nombre`, `Apellido`, `Email`, `Clave`, `Estatus`) VALUES
(1, 1, 'Ricardo   ', 'Palacios   ', 'admincorreo@educacion.cdmx.gob.mx', 'adminsectei', 1),
(2, 2, 'Angélica      ', 'Antonio Monroy     ', 'angelica.antonio@educacion.cdmx.gob.mx', '123', 1),
(3, 2, 'Veronica        ', 'Juarez Campos        ', 'veronica.juarez@educacion.cdmx.gob.mx', '123', 1),
(4, 3, 'Alin      ', 'Celis Téllez   ', 'alin.celis@educacion.cdmx.gob.mx', '123', 1),
(5, 4, 'invitado        ', 'invitado  ', 'invitado@educacion.cdmx.gob.mx', '123', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `DetalleSalida`
--
ALTER TABLE `DetalleSalida`
  ADD PRIMARY KEY (`idDetalleSalida`),
  ADD KEY `idLibro` (`idLibro`),
  ADD KEY `idSalida` (`idSalida`);

--
-- Indices de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  ADD PRIMARY KEY (`idDetalleTempSalida`),
  ADD KEY `libro-detalleTemp` (`idLibro`);

--
-- Indices de la tabla `Donatario`
--
ALTER TABLE `Donatario`
  ADD PRIMARY KEY (`idDonatario`);

--
-- Indices de la tabla `Entrada`
--
ALTER TABLE `Entrada`
  ADD PRIMARY KEY (`idEntrada`),
  ADD KEY `idLibro` (`idLibro`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `Libro`
--
ALTER TABLE `Libro`
  ADD PRIMARY KEY (`idLibro`),
  ADD KEY `idProveedor` (`idProveedor`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `Proveedor`
--
ALTER TABLE `Proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `Rol`
--
ALTER TABLE `Rol`
  ADD PRIMARY KEY (`idRol`);

--
-- Indices de la tabla `Salida`
--
ALTER TABLE `Salida`
  ADD PRIMARY KEY (`idSalida`),
  ADD KEY `idDonatario` (`idDonatario`),
  ADD KEY `idUsuario` (`idUsuario`);

--
-- Indices de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD PRIMARY KEY (`idUsuario`),
  ADD KEY `idRol` (`idRol`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `DetalleSalida`
--
ALTER TABLE `DetalleSalida`
  MODIFY `idDetalleSalida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=191;

--
-- AUTO_INCREMENT de la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  MODIFY `idDetalleTempSalida` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- AUTO_INCREMENT de la tabla `Donatario`
--
ALTER TABLE `Donatario`
  MODIFY `idDonatario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `Entrada`
--
ALTER TABLE `Entrada`
  MODIFY `idEntrada` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=297;

--
-- AUTO_INCREMENT de la tabla `Libro`
--
ALTER TABLE `Libro`
  MODIFY `idLibro` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=296;

--
-- AUTO_INCREMENT de la tabla `Proveedor`
--
ALTER TABLE `Proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `Rol`
--
ALTER TABLE `Rol`
  MODIFY `idRol` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `Salida`
--
ALTER TABLE `Salida`
  MODIFY `idSalida` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT de la tabla `Usuario`
--
ALTER TABLE `Usuario`
  MODIFY `idUsuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `DetalleSalida`
--
ALTER TABLE `DetalleSalida`
  ADD CONSTRAINT `Libro-Salida` FOREIGN KEY (`idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Salida-Salida` FOREIGN KEY (`idSalida`) REFERENCES `Salida` (`idSalida`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `detalle_temp`
--
ALTER TABLE `detalle_temp`
  ADD CONSTRAINT `libro-detalleTemp` FOREIGN KEY (`idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Entrada`
--
ALTER TABLE `Entrada`
  ADD CONSTRAINT `Entrada-Libro` FOREIGN KEY (`idLibro`) REFERENCES `Libro` (`idLibro`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Entrada-Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Libro`
--
ALTER TABLE `Libro`
  ADD CONSTRAINT `Usuario-Libro` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `libro-proveedor` FOREIGN KEY (`idProveedor`) REFERENCES `Proveedor` (`idProveedor`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Salida`
--
ALTER TABLE `Salida`
  ADD CONSTRAINT `Salida-Donatario` FOREIGN KEY (`idDonatario`) REFERENCES `Donatario` (`idDonatario`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Salida-Usuario` FOREIGN KEY (`idUsuario`) REFERENCES `Usuario` (`idUsuario`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `Usuario`
--
ALTER TABLE `Usuario`
  ADD CONSTRAINT `rol-usuario` FOREIGN KEY (`idRol`) REFERENCES `Rol` (`idRol`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
