<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <title>vale de salida</title>
    <!-- <link rel="stylesheet" href="style.css"> -->
	<style>

@import url('fonts/BrixSansRegular.css');
@import url('fonts/BrixSansBlack.css');

*{
	margin: 0;
	padding: 0;
	box-sizing: border-box;
}
p, label, span, table{
	font-family: 'BrixSansRegular';
	font-size: 9pt;
}
.h2{
	font-family: 'BrixSansBlack';
	font-size: 16pt;
}
.h3{
	font-family: 'BrixSansBlack';
	font-size: 12pt;
	display: block;
	background-color:#56b40d;
	color: #FFF;
	text-align: center;
	padding: 3px;
	margin-bottom: 5px;
}
#page_pdf{
	width: 95%;
	margin: 15px auto 10px auto;
}

#factura_head, #factura_cliente, #factura_detalle{
	width: 100%;
	margin-bottom: 10px;
}
.logo_factura{
	width: 25%;
}
.info_empresa{
	width: 50%;
	text-align: center;
}
.info_factura{
	width: 25%;
}
.info_cliente{
	width: 100%;
}
.datos_cliente{
	width: 100%;
}
.datos_cliente tr td{
	width: 50%;
}
.datos_cliente{
	padding: 10px 10px 0 10px;
}
.datos_cliente label{
	width: 75px;
	display: inline-block;
}
.datos_cliente p{
	display: inline-block;
}

.textright{
	text-align: right;
}
.textleft{
	text-align: left;
}
.textcenter{
	text-align: center;
}
.round{
	border-radius: 10px;
	border: 1px solid #05817d;
	overflow: hidden;
	padding-bottom: 15px;
}
.round p{
	padding: 0 15px;
}

#factura_detalle{
	border-collapse: collapse;
}
#factura_detalle thead th{
	background-color: #56b40d;
	color: #FFF;
	padding: 5px;
}
#detalle_productos tr:nth-child(even) {
    background: #ededed;
}
#detalle_totales span{
	font-family: 'BrixSansBlack';
}
.nota{
	font-size: 12pt;
	
}
.label_gracias{
	font-family: verdana;
	font-weight: bold;
	font-style: italic;
	text-align: center;
	margin-top: 20px;
}
	
#factura_footer{
	width: 100%;
	font-size: 10pt;
	margin-top: 60px;
	text-align: center;
}
.entrega{
	width: 40%;

}
.recibe{
	width: 40%;
}
	</style>
</head>
<body>
<!-- <?php //echo $anulada; ?> -->
<div id="page_pdf">
    <table id="factura_head">
        <tr>
            <td class="logo_factura">
                <div>
                    <img src="img/logo1.png" width="200px">
                </div>
            </td>
            <td class="info_empresa">
                <span class="h2">Secteraría de Educación Ciencia Tecnología e Innovación</span>
                    <p>Direccion de Acervo Bibliohemeorográfico</p>
                    <p>Dirección: Nezahualcoyotl 127,piso 3; Centro C.P. 06080, Cuauhtémoc CDMX</p>
                    <!-- <p>NIT: <?php //echo $configuracion['nit']; ?></p> -->
                    <p>Teléfono: 51340770 EXT 1310</p>
                    <p>Email: veronica.juarez@educacion.cdmx.gob.mx</p>
                </div>
            </td>
            <td class="info_factura">
                <div class="round">
                    <span class="h3">Donación</span>
                    <p>Vale <strong><?php echo $factura['idSalida']; ?></strong></p>
                    <p>Fecha: <?php echo $factura['fecha']; ?></p>
                    <!-- <p>Hora: <?php //echo $factura['hora']; ?></p> -->
                    <p>Autoriza: <?php echo $factura['autoriza']; ?></p>
                </div>
            </td>
        </tr>
    </table>
    <table id="factura_cliente">
        <tr>
            <td class="info_cliente">
                <div class="round">
                    <span class="h3">Donatario</span>
                    <table class="datos_cliente">
                        <tr>
                            <td><label>Nombre:</label> <p><?php echo $factura['Nombre']; ?></p></td>
                            <!-- <td><label>ID:</label><p><?php //echo $factura['idDonatario']; ?></p></td> -->
                            <td><label>Teléfono:</label> <p><?php echo $factura['Telefono']; ?></p></td>
                        </tr>
                        <tr>
                            <td><label>Dirección:</label> <p><?php echo $factura['Direccion']; ?></p></td>
                        </tr>
                        <tr>
                        <td><label>Contacto:</label> <p><?php echo $factura['Contacto']; ?></p></td>
                        </tr>
						<tr>
                        <td><label>Motivo:</label> <p><?php echo $factura['Motivo']; ?></p></td>
                        </tr>
                    </table>
                </div>
            </td>

        </tr>
    </table>

    <table id="factura_detalle">
            <thead>
                <tr>
                    <!-- <th width="50px">Cant.</th> -->
                    <th class="textcenter">Título</th>
                    <th class="textcenter">Colección</th>
                    <!-- <th class="textright" width="150px">Precio Unitario.</th> -->
                    <th class="textright" width="150px"> Cantidad</th>
                </tr>
            </thead>
            <tbody id="detalle_productos">

            <?php

                if($result_detalle > 0){
				$totallib = 0;
                    while ($row = mysqli_fetch_assoc($query_productos)){
             ?>
                <tr>
                    <!-- <td class="textcenter"><?php //echo $row['cantidad']; ?></td> -->
                    <td class="textcenter"><?php echo $row['Titulo']; ?></td>
                    <td class="textcenter"><?php echo $row['Coleccion']; ?></td>
                    <!-- <td class="textright"><?php //echo $row['precio_venta']; ?></td> -->
                    <td class="textright"><?php echo $row['Cantidad']; ?></td>
                </tr>
            <?php
                      $totallib = $totallib +  $row['Cantidad'];
                    //  $subtotal = round($subtotal + $precio_total, 2);
                    }
                }

                //$impuesto     = round($subtotal * ($iva / 100), 2);
                //$tl_sniva     = round($subtotal - $impuesto,2 );
                //$total        = round($tl_sniva + $impuesto,2);
            ?>
            </tbody>
            <tfoot id="detalle_totales">
                
                <tr>
                    <td colspan="2" class="textright"><span>Total de libros</span></td>
                    <td class="textright"><span><?php echo $totallib; ?></span></td>
                </tr>
        </tfoot>
    </table>
	<table id="factura_footer">
				<tr>
				<td class="entrega">
					<p class="nota">Nombre y firma</p>	
					<p class="nota">Entrega</p>
				</td>
				<td class="recibe">
					<p class="nota">Nombre y firma</p>			
					<p class="nota">Recibe</p>
				</td>
				</tr>
	
	</table>

    <!-- <div>
        <p class="nota">Recibe</p>
		<p class="nota">Nombre y firma</p>
    </div> -->

</div>

</body>
</html>
