<?php require_once "vistas/header.php";?>
<?php
//session_start();
include '../conexion.php';
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2 and $_SESSION['rol'] !=3) {
    header("location: ../");
}

        ////////actualizar informacion del libro//////
    if(!empty($_POST))
    {
        /* print_r($_FILES);
        exit; */
        $alert2 ='';
        if (empty($_POST['titulo'])||empty($_POST['editorial'])||empty($_POST['año'])|| $_POST['id'] ||empty($_POST['adquisicion'])||empty($_POST['proveedor'])||empty($_POST['foto_actual'])||empty($_POST['foto_remove'])) {
            
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            
            $idlibro         = $_POST['idlibro']; 
            $titulo         = $_POST['titulo'];
            $autor          = $_POST['autor'];
            $ilustrador     = $_POST['ilustrador'];
            $editorial      = $_POST['editorial'];
            $coleccion      = $_POST['coleccion'];
            $año            = $_POST['año'];
            $paginas        = $_POST['paginas'];
            $formato        = $_POST['formato'];
            $adquisicion    = $_POST['adquisicion'];
            $recibe         = $_POST['recibe'];
            $estatus        = $_POST['estatus'];
            $ejemplares     = $_POST['ejemplares'];
            $proveedor      = $_POST['proveedor'];
            $imgportada     = $_POST['foto_actual'];
            $imgremove      = $_POST['foto_remove'];
            $idusuario      = $_SESSION['idUsuario'];

            //$file           = $_FILES['foto'];
            $nombre_imagen     = $_FILES['foto']['name'];
            $tipo_imagen       = $_FILES['foto']['type'];
            $url_temp          = $_FILES['foto']['tmp_name'];
            //$carpeta        = '/img';

            $upd= '';

            if ($nombre_imagen != '') 
            {
                $destino    = 'portadas/';
                $img_nombre = 'DAB'.$nombre_imagen;
                $imgportada = $img_nombre;
                $src        = $destino.$imgportada;

                //move_uploaded_file($url_temp,$src);
                //copy ($url_temp,$src);
            }else {
                    if ($_POST['foto_actual'] != $_POST['foto_remove']) {
                        $imgportada = 'img_portada.png';                    }
            }


            //verifica  si hay un registro con el mismo nombre 
            $query = mysqli_query($conexion,"SELECT * FROM Libro WHERE Titulo = '$titulo' AND idLibro != $idlibro");
            //mysqli_close($conexion);
            $result = mysqli_fetch_array($query);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El Nombre del libro ya esta registrado</p>';
            }else{
                
                //query de insercion de datos 
                //if (empty($_POST['estatus']) && empty($_POST['recibe'])) {
                    if ($_SESSION['rol']==1) {
                        $query_insert = mysqli_query($conexion,"UPDATE Libro 
                        SET idProveedor = '$proveedor',Titulo ='$titulo',Autor = '$autor',Ilustrador = '$ilustrador',Editorial = '$editorial',Coleccion = '$coleccion',Año = '$año',Paginas = '$paginas',Formato ='$formato',Adquisicion= '$adquisicion',Recibe ='$recibe',Estatus='$estatus',Ejemplares = '$ejemplares',Portada = '$imgportada'
                        WHERE idLibro = $idlibro");
                    }else{
                        $query_insert = mysqli_query($conexion,"UPDATE Libro 
                        SET idProveedor = '$proveedor',Titulo ='$titulo',Autor = '$autor',Ilustrador = '$ilustrador',Editorial = '$editorial',Coleccion = '$coleccion',Año = '$año',Paginas = '$paginas',Formato ='$formato',Adquisicion= '$adquisicion',Portada = '$imgportada'
                        WHERE idLibro = $idlibro");
                    }
                
                if ($query_insert) {
                    
                    if (($nombre_imagen != '' && ($_POST['foto_actual']!= 'img_portada.png')) || ($_POST['foto_actual'] != $_POST['foto_remove'])) {
                        unlink('portadas/'.$_POST['foto_actual']);
                    }

                    if ($nombre_imagen != ''){
                        move_uploaded_file($url_temp,$src);
                        /* copy ($tmpfoto,$urlnueva); */
                        //header('location: ListaDonatarios.php');
                    }
                    $alert2 ='<p class = "msg_save">Libro actualizado correctamente</p>';
                    header('location: ListaLibros.php');
                }else{
                    $alert2 ='<p class = "msg_error">Error al actualizar el libro</p>';
                }
            }
        }
    
    }



//Motrar Datos 
if(empty($_REQUEST['id'])) 
{
    header('Location: ListaLibros.php');
    //mysqli_close($conexion);
}else{
    $id_libro = mysqli_real_escape_string($conexion, $_REQUEST['id']);
    //$id_libro = $_REQUEST['id'];
    if (!is_numeric($id_libro)){
        header('Location: ListaLibros.php');
        }
    //mysqli_close($conexion);
    if ($_SESSION['rol']==1) {
        $sql = mysqli_query($conexion,"SELECT l.idLibro, p.idProveedor, p.nombre,l.Titulo,l.Autor,l.Ilustrador,l.Editorial,l.Coleccion,l.Año,l.Paginas,l.Formato,l.Adquisicion,l.Recibe,l.Portada,l.Estatus, l.Ejemplares FROM Libro l INNER JOIN Proveedor p  ON l.idProveedor = p.idProveedor WHERE idLibro = $id_libro");
        
    }else{
        $sql = mysqli_query($conexion,"SELECT l.idLibro, p.idProveedor, p.nombre,l.Titulo,l.Autor,l.Ilustrador,l.Editorial,l.Coleccion,l.Año,l.Paginas,l.Formato,l.Adquisicion,l.Portada FROM Libro l INNER JOIN Proveedor p  ON l.idProveedor = p.idProveedor WHERE idLibro = $id_libro AND l.Estatus = 1");
    }
    
    //mysqli_close($conexion);
    
    $result_sql= mysqli_num_rows($sql);
    $foto='';
    $classRemove ='notBlock';
    if ($result_sql > 0) {
        $data_libro =  mysqli_fetch_array($sql);

        if ($data_libro['Portada'] != 'img_portada.png' ){
            $classRemove = '';
            $foto ='<img id="img" src="portadas/'.$data_libro['Portada'].'" alt="Libro">';
        }
        //print_r($data_libro);
        //exit;
        
    }else{
        header('Location: ListaLibros.php');
        /*
        while($data = mysqli_fetch_array($sql)){
            $idlibro = $data['idDonatario'];
            $nombre = $data['Nombre'];
            $contacto = $data['Contacto'];
            $direccion = $data['Direccion'];
            $telefono = $data['Telefono'];
            $email = $data['Email'];
            $estatus = $data['Estatus'];
        }*/
    }
    
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar libro</title>
<?php include "vistas/scripts.php";?>
</head>
<body>

<section id= "contenedor">

    <div class="form_register">
        <h1>Actualizar registro de libro</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post" enctype="multipart/form-data">
        <input type="hidden" name="idlibro" value ="<?php echo $data_libro['idLibro'] ;?>">
        <input type="hidden" id="foto_actual" name="foto_actual" value="<?php echo $data_libro['Portada'] ;?>">
        <input type="hidden" id="foto_remove" name="foto_remove" value="<?php echo $data_libro['Portada'] ;?>">

        <label for="titulo">Titulo</label>
        <input type="text" name="titulo" id="titulo" placeholder="Titulo" value="<?php echo $data_libro['Titulo'] ;?>" >
        <label for="autor">Autor</label>
        <input type="text" name ="autor" id="autor" placeholder="Autor" value="<?php echo $data_libro['Autor'] ;?>" >
        <label for="ilustrador">Ilustrador</label>
        <input type="text" name ="ilustrador" id="ilustrador" placeholder="Ilustrador" value="<?php echo $data_libro['Ilustrador'] ;?>">
        <label for="editorial">Editorial</label>
        <input type="text" name ="editorial" id="editorial" placeholder="Editorial" value="<?php echo $data_libro['Editorial'] ;?>">
        <label for="coleccion">Colección</label>
        <input type="text" name ="coleccion" id="coleccion" placeholder="Coleccion" value="<?php echo $data_libro['Coleccion'] ;?>">
        <label for="año">Año</label>
        <input type="number" name="año" id="año" placeholder="Eje.:1999" value="<?php echo $data_libro['Año'] ;?>">
        <label for="paginas">Paginas</label>
        <input type="number" name="paginas" id="paginas" placeholder="Paginas" value="<?php echo $data_libro['Paginas'] ;?>">
        <label for="formato">Formato</label>
        <select name="formato" id="formato" class="NotItemOne">
            <option value="<?php echo $data_libro['Formato'] ;?>"><?php echo $data_libro['Formato'] ;?></option>
            <option value=""></option>
            <option value="Audiolibro">Audiolibro</option>
            <option value="Braille">Braille</option>
        </select>
        <label for="adquisicion">Aquisición</label>
        <select name="adquisicion" id="adquisicion" class="NotItemOne">
            <option value="<?php echo $data_libro['Adquisicion'] ;?>"><?php echo $data_libro['Adquisicion'] ;?></option>
            <option value="Donación">Donación</option>
            <option value="Compra">Compra</option>
        </select>
        <?php if ($_SESSION['rol']==1)
        {?>
        <label for="recibe">Recibe en bodega</label>
        <input type="text" name="recibe" id="recibe" placeholder="Recibe en bodega"value="<?php echo $data_libro['Recibe'] ;?>">
        <?php }?>   
        <?php if ($_SESSION['rol']==1)
        {?>
        <label for="estatus">Estatus</label>
        <input type="number" name="estatus" id="estatus" placeholder="Estatus" value = "<?php echo $data_libro['Estatus'] ;?>">
        <?php }?> 
        <?php if ($_SESSION['rol']==1)
        {?>
        <label for="ejemplares">Ejemplares</label>
        <input type="number" name="ejemplares" id="ejemplares" placeholder="Ejemplares" value = "<?php echo $data_libro['Ejemplares'] ;?>">
        <?php }?>     
        <label for="proveedor">Proveedor</label>
        <?php 
            $query_proveedor = mysqli_query($conexion,"SELECT idProveedor,Nombre FROM Proveedor WHERE Estatus = 1 ORDER BY Nombre ASC");
            $result_proveedor = mysqli_num_rows($query_proveedor);
            mysqli_close($conexion);

        ?>
        <select name="proveedor" id="proveedor" class="NotItemOne">
            <option value="<?php echo $data_libro['idProveedor'] ;?>" selected><?php echo $data_libro['nombre'] ;?> </option>
            <?php 
            if ($result_proveedor>0) {
                while($proveedor = mysqli_fetch_array($query_proveedor)){

            ?>
                <option value="<?php echo $proveedor ['idProveedor'] ;?>"><?php echo $proveedor ['Nombre'] ;?></option>
                
            <?php
                }
            }

            ?>
        </select>

        <div class="photo">
            <label for="foto" class ="ufoto">Portada</label>
                <div class="prevPhoto">
                    <span class="delPhoto <?php echo $classRemove ;?>">X</span>
                    <label for="foto"></label>
                    <?php echo $foto  ;?>
                </div>
                <div class="upimg">
                    <input type="file" name="foto" id="foto">
                </div>
                <div id="form_alert"></div>
                </div>

        
        <input type="submit" value="Actualizar libro" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php" ?>
</html>