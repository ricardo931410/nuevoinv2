<?php

include "../conexion.php"; 
require "platillaReporteLibro.php";
//require "fpdf/fpdf.php";

if (!empty($_POST['titulo'])) {
        $titulo = $_POST['titulo'];
        $sql = "SELECT idLibro, Titulo, Autor, Ilustrador, Editorial, Coleccion, Año, Paginas, Ejemplares FROM Libro WHERE Estatus = 1 AND Editorial LIKE '%$titulo%' ORDER BY idLibro DESC";
       
        $resultado = $conexion -> query($sql);
       
        
        
        $pdf = new PDF ("L","mm","letter");
        $pdf->AliasNbPages();
        $pdf -> AddPage();
        
        $pdf -> Ln(2);
        
        $pdf -> SetFont("Arial","B",9);
        $pdf -> Cell(10,5,"No.",1,0,"C");
        //$pdf -> Cell(10,5,"Vale",1,0,"C");
        $pdf -> Cell(90,5,"Titulo",1,0,"C");
        /* $pdf -> Cell(50,5,"Autor",1,0,"C"); */
        //$pdf -> Cell(40,5,"Ilustrador",1,0,"C");
        $pdf -> Cell(70,5,"Editorial",1,0,"C");
        $pdf -> Cell(70,5,"Coleccion",1,0,"C");
        /* $pdf -> Cell(15,5,utf8_decode("Año"),1,0,"C"); */
        /* $pdf -> Cell(20,5,"Paginas",1,0,"C"); */
        $pdf -> Cell(20,5,"Ejemplares",1,1,"C");
        //$pdf -> Cell(20,5,"Autorizo",1,1,"C");
        $N = 1;
        $pdf -> SetFont("Arial","",9);
        while($fila = $resultado->fetch_assoc()){
            $pdf -> Cell(10,5,$N,1,0,"C");
            //$pdf -> Cell(10,5,$fila['idLibro'],1,0,"C");
            $pdf -> Cell(90,5, utf8_decode($fila['Titulo']) ,1,0,"C");
            /* $pdf -> Cell(50,5, utf8_decode($fila['Autor']) ,1,0,"C"); */
            //$pdf -> Cell(40,5, utf8_decode($fila['Ilustrador']) ,1,0,"C");
            $pdf -> Cell(70,5, utf8_decode($fila['Editorial']) ,1,0,"C");
            $pdf -> Cell(70,5, utf8_decode($fila['Coleccion']) ,1,0,"C");
            /* $pdf -> Cell(15,5,$fila['Año'],1,0,"C"); */
            /* $pdf -> Cell(20,5,utf8_decode($fila['Paginas']),1,0,"C"); */
            $pdf -> Cell(20,5,utf8_decode($fila['Ejemplares']),1,1,"C");
            //$pdf -> Cell(20,5,$fila['Cantidad'],1,1,"C");
            //$pdf -> Cell(20,5,$fila['Autorizo'],1,1,"C");
            $N = $N+1;
            
        }
        
        $pdf -> Output();
    
    }else {

        $sql = "SELECT idLibro, Titulo, Autor, Ilustrador, Editorial, Coleccion, Año, Paginas, Ejemplares FROM Libro WHERE Estatus = 1 ORDER BY idLibro DESC";
       
        $resultado = $conexion -> query($sql);
       
        
        
        $pdf = new PDF ("L","mm","letter");
        $pdf->AliasNbPages();
        $pdf -> AddPage();
        
        $pdf -> Ln(2);
        
        $pdf -> SetFont("Arial","B",9);
        $pdf -> Cell(10,5,"No.",1,0,"C");
        //$pdf -> Cell(10,5,"Vale",1,0,"C");
        $pdf -> Cell(90,5,"Titulo",1,0,"C");
        /* $pdf -> Cell(45,5,"Autor",1,0,"C"); */
        //$pdf -> Cell(40,5,"Ilustrador",1,0,"C");
        $pdf -> Cell(70,5,"Editorial",1,0,"C");
        $pdf -> Cell(70,5,"Coleccion",1,0,"C");
        /* $pdf -> Cell(15,5,utf8_decode("Año"),1,0,"C"); */
        /* $pdf -> Cell(20,5,"Paginas",1,0,"C"); */
        $pdf -> Cell(20,5,"Ejemplares",1,1,"C");
        //$pdf -> Cell(20,5,"Autorizo",1,1,"C");
        $N = 1;
        $pdf -> SetFont("Arial","",9);
        while($fila = $resultado->fetch_assoc()){
            $pdf -> Cell(10,5,$N,1,0,"C");
            //$pdf -> Cell(10,5,$fila['idLibro'],1,0,"C");
            $pdf -> Cell(90,5, utf8_decode($fila['Titulo']) ,1,0,"C");
            /* $pdf -> Cell(45,5, utf8_decode($fila['Autor']) ,1,0,"C"); */
            //$pdf -> Cell(40,5, utf8_decode($fila['Ilustrador']) ,1,0,"C");
            $pdf -> Cell(70,5, utf8_decode($fila['Editorial']) ,1,0,"C");
            $pdf -> Cell(70,5, utf8_decode($fila['Coleccion']) ,1,0,"C");
            /* $pdf -> Cell(15,5,$fila['Año'],1,0,"C"); */
           /*  $pdf -> Cell(20,5,utf8_decode($fila['Paginas']),1,0,"C"); */
            $pdf -> Cell(20,5,utf8_decode($fila['Ejemplares']),1,1,"C");
            //$pdf -> Cell(20,5,$fila['Cantidad'],1,1,"C");
            //$pdf -> Cell(20,5,$fila['Autorizo'],1,1,"C");
            $N = $N+1;
            
        }
        
        $pdf -> Output();
    }
    
    
    ?>