<?php require_once "vistas/header.php";?>
<?php
session_start();
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2 and $_SESSION['rol'] !=3) {
    header("location: ../");
}
include '../conexion.php';
    if(!empty($_POST))
    {
        $alert2 ='';
        if (empty($_POST['nombre'])||empty($_POST['contacto'])||empty($_POST['direccion'])||empty($_POST['telefono'])||empty($_POST['email'])) {
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            $idproveedor =$_POST['idproveedor'];
            $nombre = $_POST['nombre'];
            $contacto = $_POST['contacto'];
            $direccion = $_POST['direccion'];
            $telefono = ($_POST['telefono']);
            $email = $_POST['email'];
            $estatus = $_POST['estatus'];

            $query = mysqli_query($conexion,"SELECT * FROM Proveedor 
                                            WHERE (Nombre = '$nombre' AND idProveedor != $idproveedor)");
            $result = mysqli_fetch_array($query);
            //$result = count($result);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El proveedor ya existe</p>';
            }else{
                    if (empty($_POST['estatus'])) {
                        $sql_update = mysqli_query($conexion, "UPDATE Proveedor
                                                        SET Nombre = '$nombre', Contacto = '$contacto', Direccion ='$direccion', Telefono ='$telefono', Email = '$email'
                                                        WHERE idProveedor= $idproveedor");
                    }else{
                        $sql_update = mysqli_query($conexion, "UPDATE Proveedor
                                                        SET Nombre = '$nombre', Contacto = '$contacto', Direccion ='$direccion', Telefono ='$telefono', Email = '$email', Estatus = '$estatus'
                                                        WHERE idProveedor= $idproveedor");

                    }
                }
                if ($sql_update) {
                    $alert2 ='<p class = "msg_save">Proveedor actualizado correctamente</p>';
                    header('location: ListaProveedores.php');
                }else{
                    $alert2 ='<p class = "msg_error">Error al actualizar el proveedor</p>';
                }
            }
        }

    //Mostrar datos

    if(empty($_REQUEST['id'])) 
    {
        header('Location: Listaproveedores.php');
        mysqli_close($conexion);
    }
            //$idproveedor = $_REQUEST['id'];
            $idproveedor = mysqli_real_escape_string($conexion, $_REQUEST['id']);
                if ($_SESSION['rol']==1)
                {
                    $sql=mysqli_query($conexion,"SELECT idProveedor, Nombre,Contacto,Direccion,Telefono,Email,Estatus From Proveedor WHERE idProveedor = $idproveedor");
                }else{
                    $sql=mysqli_query($conexion,"SELECT idProveedor, Nombre,Contacto,Direccion,Telefono,Email,Estatus From Proveedor WHERE idProveedor = $idproveedor and Estatus =1 ");
                }

                mysqli_close($conexion);

                $result_sql= mysqli_num_rows($sql);
                if ($result_sql == 0) {
                        header('Location: ListaProveedores.php');
        
                    }else{
                        while($data = mysqli_fetch_array($sql)){
                            $idproveedor = $data['idProveedor'];
                            $nombre = $data['Nombre'];
                            $contacto = $data['Contacto'];
                            $direccion = $data['Direccion'];
                            $telefono = $data['Telefono'];
                            $email = $data['Email'];
                            $estatus = $data['Estatus'];
                    }
                }

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar proveedor</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>

<section id= "contenedor">

    <div class="form_register">
        <h1>Actualizar proveedor</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post">
        <input type="hidden" name="idproveedor" value="<?php echo $idproveedor  ;?>">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre" value = "<?php echo $nombre; ?> ">
        <label for="contacto">Contacto</label>
        <input type="text" name ="contacto" id="contacto" placeholder="Contacto" value = "<?php echo $contacto; ?> ">
        <label for="direccion">Dirección</label>
        <input type="texto" name="direccion" id="direccion" placeholder="Dirección" value = "<?php echo $direccion; ?> ">
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" id="telefono" placeholder="telefono" value = "<?php echo $telefono; ?> ">
        <label for="email">Correo electronico</label>
        <input type="texto" name="email" id="email" placeholder="email" value = "<?php echo $email; ?> ">

        <?php if ($_SESSION['rol']==1)
        {?>
        <label for="estatus">Estatus</label>
        <input type="text" name="estatus" id="estatus" placeholder="estatus" value = "<?php echo $estatus;?> ">
        <?php }?>   

        </select>
        <input type="submit" value="Actualizar proveedor" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php"; ?>
</html>


