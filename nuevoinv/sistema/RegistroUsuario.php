<?php include "vistas/header.php";?>
<?php

session_start();
if ($_SESSION['rol'] !=1 ) {
    header("location: ../");
}

include '../conexion.php';
    if(!empty($_POST))
    {
        $alert2 ='';
        if (empty($_POST['nombre'])|| empty($_POST['apellido'])||empty($_POST['email'])||empty($_POST['clave'])||empty($_POST['rol'])) {
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            
            $nombre = $_POST['nombre'];
            $apellido = $_POST['apellido'];
            $email = $_POST['email'];
            $clave = $_POST['clave'];
            $rol = $_POST['rol'];

            $query = mysqli_query($conexion,"SELECT * FROM Usuario WHERE Email = '$email'");
            //mysqli_close($conexion);
            $result = mysqli_fetch_array($query);
            if ($result > 0) {
                $alert2 ='<p class = "msg_error">El Usuario ya existe</p>';
            }else{
                $query_insert = mysqli_query($conexion,"INSERT INTO Usuario(Nombre,Apellido,Email,Clave,idRol) 
                VALUES('$nombre','$apellido','$email','$clave','$rol')");
                if ($query_insert) {
                    $alert2 ='<p class = "msg_save">Usuario registrado correctamente</p>';
                    header('Location: ListaUsuarios.php');
                }else{
                    $alert2 ='<p class = "msg_error">Error al registrar el Usuario</p>';
                }
            }
        }
    }

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de usuario</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>

<section id= "contenedor">

    <div class="form_register">
        <h1>Registro de Usuario</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre">
        <label for="apellido">Apellido</label>
        <input type="text" name ="apellido" id="apellido" placeholder="Apellido">
        <label for="email">Correo electronico</label>
        <input type="email" name="email" id="email" placeholder="Correo electronico">
        <label for="clave">Contraseña</label>
        <input type="password" name="clave" id="clave" placeholder="Contraseña">
        <label for="rol">Rol de usuario</label>
        <?php
            $query_rol = mysqli_query($conexion,"SELECT * FROM Rol");
            mysqli_close($conexion);
            $result_rol = mysqli_num_rows($query_rol);

           
        ?>

        <select name="rol" id="rol">
            <?php
                if ($result_rol > 0 ) {
                    while( $rol = mysqli_fetch_array($query_rol)){
            ?>
                <option value="<?php echo $rol['idRol'];  ?>"><?php echo $rol['rol'] ;  ?> </option>
            <?php
                    }
                }
            ?>

        </select>
        <input type="submit" value="Registrar Usuario" class="btn-save">

    </form>


</div>


</section>

<?php require_once "vistas/footer.php" ?>
    
</body>
</html>

