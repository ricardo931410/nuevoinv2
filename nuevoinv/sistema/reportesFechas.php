<?php

include "../conexion.php"; 
require "platillaReporte.php";
//require "fpdf/fpdf.php";

    if (!empty($_POST['fecha_de']) && !empty($_POST['fecha_a'])) {

        $fecha_d = $_POST['fecha_de'];
        $fecha_a = $_POST['fecha_a'];
        
        $sql = "SELECT ds.idDetalleSalida, l.Titulo,(s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
    INNER JOIN Salida s
    ON ds.idSalida = s.idSalida
    INNER JOIN Libro l
    ON ds.idLibro = l.idLibro
    INNER JOIN Donatario d
    ON s.idDonatario = d.idDonatario
    INNER JOIN Usuario u 
    ON s.idUsuario = u.idUsuario
    WHERE Fecha BETWEEN '$fecha_d' AND '$fecha_a'
    ORDER BY s.idSalida DESC";
    $resultado = $conexion -> query($sql);
    
    
    //print_r($_POST['fecha_a']);
    //exit;
    
    
    $pdf = new PDF ("L","mm","letter");
    $pdf->AliasNbPages();
    $pdf -> AddPage();
    
    $pdf -> Ln(2);
    
    $pdf -> SetFont("Arial","B",9);
    $pdf -> Cell(10,5,"No.",1,0,"C");
    //$pdf -> Cell(10,5,"Vale",1,0,"C");
    $pdf -> Cell(60,5,"Titulo",1,0,"C");
    $pdf -> Cell(30,5,"Fecha",1,0,"C");
    $pdf -> Cell(70,5,"Donatario",1,0,"C");
    $pdf -> Cell(70,5,"Motivo",1,0,"C");
    $pdf -> Cell(20,5,"Cantidad",1,1,"C");
    //$pdf -> Cell(20,5,"Autorizo",1,1,"C");
    
    $pdf -> SetFont("Arial","",9);
    $N = 1;
    while($fila = $resultado->fetch_assoc()){
        $pdf -> Cell(10,5,$N,1,0,"C");
        //$pdf -> Cell(10,5,$fila['Vale'],1,0,"C");
        $pdf -> Cell(60,5, utf8_decode($fila['Titulo']) ,1,0,"C");
        $pdf -> Cell(30,5,$fila['Fecha'],1,0,"C");
        $pdf -> Cell(70,5,utf8_decode($fila['Nombre']),1,0,"C");
        $pdf -> Cell(70,5,utf8_decode($fila['Motivo']),1,0,"C");
        $pdf -> Cell(20,5,$fila['Cantidad'],1,1,"C");
        //$pdf -> Cell(20,5,$fila['Autorizo'],1,1,"C");
        $N = $N+1;
        
    }
    
    $pdf -> Output();
}else {
    $sql = "SELECT ds.idDetalleSalida, l.Titulo,(s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
    INNER JOIN Salida s
    ON ds.idSalida = s.idSalida
    INNER JOIN Libro l
    ON ds.idLibro = l.idLibro
    INNER JOIN Donatario d
    ON s.idDonatario = d.idDonatario
    INNER JOIN Usuario u 
    ON s.idUsuario = u.idUsuario
    ORDER BY s.idSalida DESC";
    $resultado = $conexion -> query($sql);
    
    
    //print_r($_POST['fecha_a']);
    //exit;
    
    
    $pdf = new PDF ("L","mm","letter");
    $pdf->AliasNbPages();
    $pdf -> AddPage();
    
    $pdf -> Ln(2);
    
    $pdf -> SetFont("Arial","B",9);
    $pdf -> Cell(10,5,"No.",1,0,"C");
    //$pdf -> Cell(10,5,"Vale",1,0,"C");
    $pdf -> Cell(60,5,"Titulo",1,0,"C");
    $pdf -> Cell(30,5,"Fecha",1,0,"C");
    $pdf -> Cell(70,5,"Donatario",1,0,"C");
    $pdf -> Cell(70,5,"Motivo",1,0,"C");
    $pdf -> Cell(20,5,"Cantidad",1,1,"C");
    //$pdf -> Cell(20,5,"Autorizo",1,1,"C");
    
    $pdf -> SetFont("Arial","",9);
    $N = 1;
    while($fila = $resultado->fetch_assoc()){
        $pdf -> Cell(10,5,$N,1,0,"C");
        //$pdf -> Cell(10,5,$fila['Vale'],1,0,"C");
        $pdf -> Cell(60,5, utf8_decode($fila['Titulo']) ,1,0,"C");
        $pdf -> Cell(30,5,$fila['Fecha'],1,0,"C");
        $pdf -> Cell(70,5,utf8_decode($fila['Nombre']),1,0,"C");
        $pdf -> Cell(70,5,utf8_decode($fila['Motivo']),1,0,"C");
        $pdf -> Cell(20,5,$fila['Cantidad'],1,1,"C");
        //$pdf -> Cell(20,5,$fila['Autorizo'],1,1,"C");
        $N = $N+1;
        
    }
    
    $pdf -> Output();
}
    
    
    ?>