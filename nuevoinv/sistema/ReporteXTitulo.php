<?php require_once "vistas/header.php";?>
<?php
//session_start();
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2) {
	header("location: ../");
 }

include "../conexion.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "vistas/scripts.php" ;?>
    <title>Lista de donaciones</title>

    <style>
        .form_serch_date{
	padding: 10px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	margin: 10px auto;
}
.form_serch_date label{
	margin: 0 10px;
}

.form_serch_date input{
	width: auto;
}
.form_serch_date .btn_view{
	padding: 8px;
}

.btn_view{
	background-color: #5dc262;
	border: 0;
	border-radius: 10px;
	cursor: pointer;
	padding: 10px;
	margin: 0 3px;
	color: white;
}
    </style>
</head>
<body>


<section id="contenedor">
    
    <h1>Lista de donaciones</h1>
        <div>
        <h5>Buscar por título</h5>
        <form action="reportesTitulo.php" method="post" class="form_serch_date" autocomplete="off">
            <label> De: </label>
            <input type="date" name="fecha_de" id="fecha_de" requiered>
            <label> a </label>
            <input type="date" name="fecha_a" id="fecha_a" requiered>
			<!-- <input type="submit" name="generar_reporte"> -->
            <label for="titulo"></label>
            <input width="300px" type="text" id="titulo" name="titulo" placeholder="Título">
            <button type="submit" class="btn_view" name="generar_reporte"> Generar reporte PDF</button>
        
        </form>       
        </div>
        <div>
        <form action="reporteTituloExcel.php" method="post" class="form_serch_date" autocomplete="off">
            <label> De: </label>
            <input type="date" name="fecha_de" id="fecha_de" requiered>
            <label> a </label>
            <input type="date" name="fecha_a" id="fecha_a" requiered>
			<!-- <input type="submit" name="generar_reporte"> -->
            <label for="titulo"></label>
            <input type="text" id="titulo" name="titulo" placeholder="Título" > 
            <button type="submit" class="btn_view" name="generar_reporte"> Generar reporte Excel </button>
        
        </form>       
        </div>


    <table>
        <tr>
            <th>Vale</th>
            <th>Título</th>
            <th>Fecha</th>
            <th>Donatario</th>
            <th>Motivo</th>
			<th>Cantidad</th>
			<th>Autorizó</th>
        </tr>
    <?php
    
    //paginador
    //cuantos registros estan activos
    $sql_registe =mysqli_query($conexion, "SELECT COUNT(*) AS total_registro FROM DetalleSalida");

    //guarda el resultado en un array
    $result_register = mysqli_fetch_array($sql_registe);
    //guarda en una variable el numero total de registros
    $total_registro = $result_register['total_registro'];

    //variable que contiene el  numero de registros por pagina
    $por_pagina = 100;

    //validacion de el paginador manda por el url
    if(empty($_GET['pagina']))
    {
        $pagina =1;
    }else{
        $pagina = $_GET['pagina'];
    }

    $desde = ($pagina-1) * $por_pagina;
    $total_paginas = ceil($total_registro / $por_pagina); 

    //el query retorna la informacion el Limit indica desde donde va a iniciar y hasta que regristro va a limitar
        
        //$query =mysqli_query($conexion,"SELECT idDonatario, Nombre, Contacto,Direccion,Telefono, Email,FechaAlta FROM Donatario WHERE Estatus = 1 LIMIT $desde,$por_pagina");

        $query = mysqli_query($conexion,"SELECT ds.idDetalleSalida, l.Titulo, (s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
		INNER JOIN Salida s
		ON ds.idSalida = s.idSalida
		INNER JOIN Libro l
		ON ds.idLibro = l.idLibro
		INNER JOIN Donatario d
		ON s.idDonatario = d.idDonatario
		INNER JOIN Usuario u 
		ON s.idUsuario = u.idUsuario
		ORDER BY s.idSalida DESC LIMIT $desde,$por_pagina");

		//WHERE s.Fecha BETWEEN '2021-03-15' AND '2021-03-30'


    //mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            ?>
                <tr id ="row_<?php //echo $data['idLibro']  ;?> ">
				<td><?php echo $data['Vale']   ;?></td>
                    <td><?php echo $data['Titulo']   ;?></td>
                    <td><?php echo $data['Fecha']   ;?></td>
                    <td><?php echo $data['Nombre']; ?></td>
                    <td><?php echo $data['Motivo']   ;?></td>
					<td><?php echo $data['Cantidad']   ;?></td>
					<td><?php echo $data['Autorizo']   ;?></td>
                </tr>
       <?php     
        }


    }


    ?>

    </table>
    <div class="paginador">
        <ul>
            <?php
                if($pagina !=1)
                {
            ?>
            <li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
            <li><a href="?pagina=<?php echo $pagina -1;?>"><<<</a></li>
            <?php
            }
            ?>
            <?php
            for ($i=1; $i <= $total_paginas; $i++) { 
                if($i == $pagina){
                    echo '<li class="pageselected">'.$i.'</li>';
                }else{
                    echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
                }
            }
            ?>
            <?php
            if($pagina !=$total_paginas){
            ?>
            
            <li><a href="?pagina=<?php echo $pagina + 1 ;?>">>>></a></li>
            <li><a href="?pagina=<?php echo $total_paginas; ;?>">>|</a></li>
            <?php
            } 
            ?>

        </ul>
    </div>

</section>
    
</body>
<?php include "vistas/scripts.php" ;?>
<?php require_once "vistas/footer.php" ?>
</html>