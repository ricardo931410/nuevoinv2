<?php
//session_start();
//if ($_SESSION['rol'] !=1 ) {
//    header("location: ../");
//}
include "../conexion.php";
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "vistas/scripts.php" ;?>
    <title>Lista de proveedores</title>
</head>
<body>
    
<?php require_once "vistas/header.php";?>

<section id="contenedor">
    
    <h1>Lista de proveedores</h1>
    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
    <a href="RegistroProveedor.php" class="btn_new">Registrar proveedor</a>
    <?php }?>
    <form action="BuscarProveedor.php" method="get" class="form_buscar">
        <input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
        <input type="submit" value="Buscar" class="btn_buscar" name="" id="">
    </form>

    <table>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Contacto</th>
            <th>Dirección</th>
            <th>Teléfono</th>
            <th>Correo electrónico</th>
            <!-- <th>Fecha de registro</th> -->
            <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
            <th>Acciones</th>
            <?php }?>
        </tr>
    <?php
    
    //paginador
    //cuantos registros estan activos
    $sql_registe =mysqli_query($conexion, "SELECT COUNT(*) AS total_registro FROM Proveedor");

    //guarda el resultado en un array
    $result_register = mysqli_fetch_array($sql_registe);
    //guarda en una variable el numero total de registros
    $total_registro = $result_register['total_registro'];

    //variable que contiene el  numero de registros por pagina
    $por_pagina = 10;

    //validacion de el paginador manda por el url
    if(empty($_GET['pagina']))
    {
        $pagina =1;
    }else{
        $pagina = $_GET['pagina'];
    }

    $desde = ($pagina-1) * $por_pagina;
    $total_paginas = ceil($total_registro / $por_pagina); 

    //el query retorna la informacion el Limit indica desde donde va a iniciar y hasta que regristro va a limitar
    if (empty($_SESSION['rol']==1)) {
        $query =mysqli_query($conexion,"SELECT idProveedor, Nombre, Contacto,Direccion,Telefono, Email FROM Proveedor WHERE Estatus = 1 ORDER BY idProveedor DESC LIMIT $desde,$por_pagina");
    
    }else{
        $query =mysqli_query($conexion,"SELECT idProveedor, Nombre, Contacto,Direccion,Telefono, Email FROM Proveedor ORDER BY idProveedor DESC LIMIT $desde,$por_pagina");
    }


    mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            ?>
                <tr>
                    <td><?php echo $data['idProveedor']   ;?></td>
                    <td><?php echo $data['Nombre'];  ?></td>
                    <td><?php echo $data['Contacto']   ;?></td>
                    <td><?php echo $data['Direccion']; ?></td>
                    <td><?php echo $data['Telefono']   ;?></td>
                    <td><?php echo $data['Email']   ;?></td>
                    <!-- <td><?php //echo $data['FechaAlta']   ;?></td> -->
                    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
                    <td>
                        <a href="EditarProveedor.php?id=<?php echo $data['idProveedor'];?>" class="link_edit">Editar</a>

                        <!-- 
                                |

                                    <a href="EliminarConfDonatario.php?id=<?php //echo $data['idDonatario'];?>" class="link_delete">Eliminar</a>
                                </td>
                            -->
                            <?php }?>
                </tr>
       <?php     
        }


    }


    ?>

    </table>
    <div class="paginador">
        <ul>
            <?php
                if($pagina !=1)
                {
            ?>
            <li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
            <li><a href="?pagina=<?php echo $pagina -1;?>"><<<</a></li>
            <?php
            }
            ?>
            <?php
            for ($i=1; $i <= $total_paginas; $i++) { 
                if($i == $pagina){
                    echo '<li class="pageselected">'.$i.'</li>';
                }else{
                    echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
                }
            }
            ?>
            <?php
            if($pagina !=$total_paginas){
            ?>
            
            <li><a href="?pagina=<?php echo $pagina + 1 ;?>">>>></a></li>
            <li><a href="?pagina=<?php echo $total_paginas; ;?>">>|</a></li>
            <?php
            } 
            ?>

        </ul>
    </div>

</section>



<?php require_once "vistas/footer.php" ?>


</body>
</html>




