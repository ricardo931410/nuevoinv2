<?php
   	session_start();
    if (empty ($_SESSION['active'])) {
    header('location: ../');
}
?>

<header>
		<div class="header">
			<!-- <img class="logo" src="assets/img/CDMXLogo.png" alt=""> -->
			<h1>Inventario DAB</h1>
			<div class="optionsBar">
				<!-- <p>Guatemala, 20 noviembre de 2017</p> -->
				<span>|</span>
				<span class="user"><?php echo $_SESSION['nombre'].' -'.$_SESSION['rol']; ?></span>
				<!-- <img class="photouser" src="img/user.png" alt="Usuario"> -->
				<a href="salir.php"><img class="close" src="assets/img/salir.png" alt="Salir del sistema" title="Salir"></a>
			</div>
		</div>
		<nav>
			<ul>
				<li><a href="index.php">Inicio</a></li>
                <?php if ($_SESSION['rol'] == 1) { ?>
				<li class="principal">
					<a href="ListaUsuarios.php">Usuarios</a>
					<ul>
						<li><a href="ListaUsuarios.php">Lista de Usuarios</a></li>
						<li><a href="RegistroUsuario.php">Nuevo Usuario</a></li>
					</ul>
				</li>
                <?php }   ?>
				<li class="principal">
					<a href="ListaDonatarios.php">Donatarios</a>
					<ul>
						<li><a href="ListaDonatarios.php">Lista de Donatario</a></li>
                    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
						<li><a href="RegistroDonatario.php">Nuevo Donatario</a></li>
                        <?php }?>
					</ul>
				</li>
				<li class="principal">
					<a href="ListaProveedores.php">Proveedores</a>
					<ul>
						<li><a href="ListaProveedores.php">Lista de Proveedores</a></li>
                    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
						<li><a href="RegistroProveedor.php">Nuevo Proveedor</a></li>
                        <?php }?>
					</ul>
				</li>
				<li class="principal">
					<a href="ListaLibros.php">Acervo</a>
					<ul>
						<li><a href="ListaLibros.php">Lista de Libros</a></li>
						<?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
						<li><a href="RegistroLibro.php">Nuevo Libro</a></li>
						<?php }?>
					</ul>
				</li>
				<?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
				<li class="principal">
					<a href="ListaEntradas.php">Adquisiciones</a>
					<ul>
						<li><a href="ListaEntradas.php">Lista de Adquisiciones</a></li>
						<!-- <li><a href="#">Nueva adquisicion</a></li> -->
					</ul>
				</li>
				<?php }?>
				<?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
				<li class="principal">
					<a href="ListaDonaciones.php">Donaciones</a>
					<ul>
						<li><a href="ListaDonaciones.php">Lista de Donaciones</a></li>
						<?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
						<li><a href="Donacion.php">Nueva donacion</a></li>
						<?php }?>
					</ul>
					<?php }?>
					<?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
					<li class="principal">
					<a href="ReporteXFechas.php">Reportes</a>
					<ul>
						<li><a href="ReporteXFechas.php">Fechas</a></li>
						<li><a href="ReporteXTitulo.php">Título</a></li>
						<li><a href="ReporteXDonatario.php">Donatario</a></li>
						<li><a href="ReporteXEditorial.php">Editorial</a></li>
						<li><a href="ReporteXLibros.php">Existencias</a></li>
					</ul>
				</li>
				<?php }?>
			</ul>
		</nav>
	</header>

<div class="modal">
	<div class="bodymodal">
	</div>
</div>