<?php include "vistas/header.php";?>
<?php
session_start();
if ($_SESSION['rol'] !=1 ) {
    header("location: ../");
}
include "../conexion.php"
?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista de Usuarios</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>


<section id="contenedor">
    
    <h1>Lista de Usuarios</h1>
    <a href="RegistroUsuario.php" class="btn_new">Registrar Usuario</a>


    <table>
        <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Apellido</th>
            <th>Usuario</th>
            <th>Rol</th>
            <th>Estatus</th>
            <th>Acciones</th>
        </tr>
    <?php

    $query =mysqli_query($conexion,"SELECT u.idUsuario, u.Nombre, u.Apellido,u.Email,u.Estatus,r.Rol FROM Usuario u INNER JOIN Rol r ON u.idRol = r.idRol  ORDER by(idUsuario)");
    mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            ?>
                <tr>
                    <td><?php echo $data['idUsuario']     ;?></td>
                    <td><?php echo $data['Nombre'];  ?></td>
                    <td><?php echo $data['Apellido']   ;?></td>
                    <td><?php echo $data['Email']; ?></td>
                    <td><?php echo $data['Rol']   ;?></td>
                    <td><?php echo $data['Estatus']   ;?></td>
                    <td>
                        <a href="editarUsuario.php?id=<?php echo $data['idUsuario'];?>" class="link_edit">Editar</a>

                        
                    </td>
                </tr>
       <?php     
        }


    }


    ?>

    </table>

</section>


<?php //require_once "vistas/admin_menu.php"; ?>

<?php require_once "vistas/footer.php" ?>

</body>
</html>