<?php
include "../conexion.php"; 
//$fecha_d = $_POST['fecha_de'];
//$fecha_a = $_POST['fecha_a'];
//$titulo = $_POST['titulo'];

if(isset($_POST['generar_reporte'])){
    if (!empty($_POST['titulo'])) {
        if (!empty($_POST['fecha_de'])) {
        $titulo = $_POST['titulo'];
        $fecha_d = $_POST['fecha_de'];
        $fecha_a = $_POST['fecha_a'];
        
        header("Content-Type: application/vnd.ms-excel; charset=iso-8859-1");
        header('Content-Disposition: attachment;filename="ReporteFechas.xls"');
        
        $sql = "SELECT ds.idDetalleSalida, l.Titulo, l.Editorial, (s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
        INNER JOIN Salida s
        ON ds.idSalida = s.idSalida
        INNER JOIN Libro l
        ON ds.idLibro = l.idLibro
        INNER JOIN Donatario d
        ON s.idDonatario = d.idDonatario
        INNER JOIN Usuario u 
        ON s.idUsuario = u.idUsuario
        WHERE l.Titulo LIKE '%$titulo%' AND 
        Fecha BETWEEN '$fecha_d' AND '$fecha_a'
        ORDER BY s.idSalida DESC";
        $resultado = $conexion -> query($sql);
            ?>

        <table border="1">
        <caption>Listado de libros</caption>
        <tr>
            <th>No.</th>
            <th>Titulo</th>
            <th>Editorial</th>
            <th>Fecha</th>
            <th>Donatario</th>
            <th>Motivo</th>
            <th>Cantidad</th>
        </tr>
        <?php $N = 1;
        while($row = mysqli_fetch_assoc($resultado)){ ?>
            <tr>
                <td><?php echo $N  ;?></td>
                <td><?php echo $row['Titulo']  ;?></td>
                <td><?php echo $row['Editorial']  ;?></td>
                <td><?php echo $row['Fecha']   ;?>  </td>
                <td><?php echo $row['Nombre']  ;?></td>
                <td><?php echo $row['Motivo']  ;?></td>
                <td><?php echo $row['Cantidad']  ;?></td>
            </tr>

        <?php 
        $N = $N + 1;

        } ;?>
        </table>


<?php }else { 
        $titulo = $_POST['titulo'];
    
        header("Content-Type: application/vnd.ms-excel; charset=iso-8859-1");
        header('Content-Disposition: attachment;filename="ReporteFechas.xls"');
        
        $sql = "SELECT ds.idDetalleSalida, l.Titulo, l.Editorial, (s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
        INNER JOIN Salida s
        ON ds.idSalida = s.idSalida
        INNER JOIN Libro l
        ON ds.idLibro = l.idLibro
        INNER JOIN Donatario d
        ON s.idDonatario = d.idDonatario
        INNER JOIN Usuario u 
        ON s.idUsuario = u.idUsuario
        WHERE l.Titulo LIKE '%$titulo%'
        ORDER BY s.idSalida DESC";
        $resultado = $conexion -> query($sql);

    ?>
                    <table border="1">
            <caption>Listado de libros</caption>
            <tr>
                <th>No.</th>
                <th>Titulo</th>
                <th>Editorial</th>
                <th>Fecha</th>
                <th>Donatario</th>
                <th>Motivo</th>
                <th>Cantidad</th>
            </tr>
            <?php $N = 1;
            while($row = mysqli_fetch_assoc($resultado)){ ?>
                <tr>
                    <td><?php echo $N  ;?></td>
                    <td><?php echo $row['Titulo']  ;?></td>
                    <td><?php echo $row['Editorial']  ;?></td>
                    <td><?php echo $row['Fecha']   ;?>  </td>
                    <td><?php echo $row['Nombre']  ;?></td>
                    <td><?php echo $row['Motivo']  ;?></td>
                    <td><?php echo $row['Cantidad']  ;?></td>
                </tr>

            <?php 
            $N = $N + 1;

            } ;?>
            </table>


            <?php } 
        }  
    }
?>
