<?php require_once "vistas/header.php";?>
<?php
//session_start();
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2) {
    header("location: ../");
}

include '../conexion.php';
    if(!empty($_POST))
    {
        /* print_r($_FILES);
        exit; */
        $alert2 ='';
        if (empty($_POST['titulo'])||empty($_POST['editorial'])||empty($_POST['año'])||empty($_POST['ejemplares'])|| $_POST['ejemplares'] <= 0||empty($_POST['adquisicion'])||empty($_POST['proveedor'])) {
            
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            
            $titulo         = $_POST['titulo'];
            $autor          = $_POST['autor'];
            $ilustrador     = $_POST['ilustrador'];
            $editorial      = $_POST['editorial'];
            $coleccion      = $_POST['coleccion'];
            $año            = $_POST['año'];
            $paginas        = $_POST['paginas'];
            $formato        = $_POST['formato'];
            $ejemplares     = $_POST['ejemplares'];
            $adquisicion    = $_POST['adquisicion'];
            $recibe         =$_POST['recibe'];
            $proveedor      = $_POST['proveedor'];
            $idusuario      = $_SESSION['idUsuario'];

            //$file           = $_FILES['foto'];
            $nombre_imagen     = $_FILES['foto']['name'];
            $tipo_imagen       = $_FILES['foto']['type'];
            $url_temp          = $_FILES['foto']['tmp_name'];
            //$carpeta        = '/img';

            $imgportada = 'img_portada.png';

            if ($nombre_imagen != '') 
            {
                $destino    = 'portadas/';
                $img_nombre = 'DAB'.$nombre_imagen;
                $imgportada = $img_nombre;
                $src        = $destino.$imgportada;

                //move_uploaded_file($url_temp,$src);
                //copy ($url_temp,$src);
            }

            
                //img/uploads/img_2423244.jpg

            //verifica  si hay un registro con el mismo nombre 
            $query = mysqli_query($conexion,"SELECT * FROM Libro WHERE Titulo = '$titulo' ");
            //mysqli_close($conexion);
            $result = mysqli_fetch_array($query);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El Nombre del libro ya esta registrado</p>';
            }else{

                //query de insercion de datos 
                $query_insert = mysqli_query($conexion,"INSERT INTO Libro(idProveedor,Titulo,Autor,Ilustrador,Editorial,Coleccion,Año,Paginas,Formato,Ejemplares,Adquisicion,Recibe,Portada,idUsuario) 
                VALUES('$proveedor','$titulo','$autor','$ilustrador','$editorial','$coleccion','$año','$paginas','$formato','$ejemplares','$adquisicion','$recibe','$imgportada','$idusuario')");

                if ($query_insert) {
                    if ($nombre_imagen != ''){
                        move_uploaded_file($url_temp,$src);
                        /* copy ($tmpfoto,$urlnueva); */
                        //header('location: ListaDonatarios.php');
                    }
                    $alert2 ='<p class = "msg_save">Libro registrado correctamente</p>';
                }else{
                    $alert2 ='<p class = "msg_error">Error al registrar el libro</p>';
                }
            }
        }
    
    }

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registrar libro</title>
<?php include "vistas/scripts.php";?>
</head>
<body>

<section id= "contenedor">

    <div class="form_register">
        <h1>Registrar libro</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post" enctype="multipart/form-data">
        <label for="titulo">Título</label>
        <input type="text" name="titulo" id="titulo" placeholder="Obligatorio">
        <label for="autor">Autor</label>
        <input type="text" name ="autor" id="autor" placeholder="opcional">
        <label for="ilustrador">Ilustrador</label>
        <input type="text" name ="ilustrador" id="ilustrador" placeholder="opcional">
        <label for="editorial">Editorial</label>
        <input type="text" name ="editorial" id="editorial" placeholder="obligatorio">
        <label for="coleccion">Colección</label>
        <input type="text" name ="coleccion" id="coleccion" placeholder="opcional">
        <label for="año">Año</label>
        <input type="number" name="año" id="año" placeholder="Eje.:1999">
        <label for="paginas">Páginas</label>
        <input type="number" name="paginas" id="paginas" placeholder="Eje.:90">
        <label for="formato">Formato</label>
        <select name="formato" id="formato">
            <option value=""></option>
            <option value="Audiolibro">Audiolibro</option>
            <option value="Braille">Braille</option>
        </select>
        <label for="ejemplares">Ejemplares</label>
        <input type="number" name="ejemplares" id="ejemplares" placeholder="Eje.:100">
        <label for="adquisicion">Aquisición</label>
        <select name="adquisicion" id="adquisicion">
            <option value="Donación">Donación</option>
            <option value="Compra">Compra</option>
        </select>
        <label for="recibe">Recibió en bodega</label>
        <input type="text" name="recibe" id="recibe" placeholder="Recibe en bodega">
        
        <!-- <label for="tipo">Tipo</label>
        <input type="tipo" name="tipo" id="tipo" placeholder="Adquisicion o donacion"> -->
        <label for="proveedor">Proveedor</label>
        <?php 
            $query_proveedor = mysqli_query($conexion,"SELECT idProveedor,Nombre FROM Proveedor WHERE Estatus = 1 ORDER BY Nombre ASC");
            $result_proveedor = mysqli_num_rows($query_proveedor);
            mysqli_close($conexion);

        ?>
        <select name="proveedor" id="proveedor">
            <?php
            if ($result_proveedor>0) {
                while($proveedor = mysqli_fetch_array($query_proveedor)){

            ?>
                <option value="<?php echo $proveedor ['idProveedor'] ;?>"><?php echo $proveedor ['Nombre'] ;?></option>
                
            <?php
                }
            }

            ?>
        </select>

        <div class="photo">
            <label for="foto" class ="ufoto">Portada</label>
                <div class="prevPhoto">
                    <span class="delPhoto notBlock">X</span>
                    <label for="foto"></label>
                </div>
                <div class="upimg">
                    <input type="file" name="foto" id="foto">
                </div>
                <div id="form_alert"></div>
                </div>

        
        <input type="submit" value="Registrar libro" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php" ?>
</html>