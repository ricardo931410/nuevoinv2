<?php
//session_start();
//if ($_SESSION['rol'] !=1 ) {
//    header("location: ../");
//}

include "../conexion.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "vistas/scripts.php" ;?>
    <title>Lista de adquisiciones</title>

    <style>
        .form_serch_date{
	padding: 10px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	margin: 10px auto;
}
.form_serch_date label{
	margin: 0 10px;
}

.form_serch_date input{
	width: auto;
}
.form_serch_date .btn_view{
	padding: 8px;
}

.btn_view{
	background-color: #5dc262;
	border: 0;
	border-radius: 10px;
	cursor: pointer;
	padding: 10px;
	margin: 0 3px;
	color: white;
}
    </style>
</head>
<body>
<?php require_once "vistas/header.php";?>


<section id="contenedor">
    
    <h1>Lista de adquisiciones</h1>
    <table>
        <tr>
            <th>ID</th>
            <th>Título</th>
            <th>Editorial</th>
            <th>Recibió</th>
            <th>Adquisición</th>
            <th>Envia</th>
            <th>Fecha</th>
            <th class="textright">Libros Recibidos</th>
           
        </tr>
    <?php
    
    //paginador
    //cuantos registros estan activos
    $sql_registe =mysqli_query($conexion, "SELECT COUNT(*) AS total_registro FROM Entrada");

    //guarda el resultado en un array
    $result_register = mysqli_fetch_array($sql_registe);
    //guarda en una variable el numero total de registros
    $total_registro = $result_register['total_registro'];

    //variable que contiene el  numero de registros por pagina
    $por_pagina = 100;

    //validacion de el paginador manda por el url
    if(empty($_GET['pagina']))
    {
        $pagina =1;
    }else{
        $pagina = $_GET['pagina'];
    }

    $desde = ($pagina-1) * $por_pagina;
    $total_paginas = ceil($total_registro / $por_pagina); 

    //el query retorna la informacion el Limit indica desde donde va a iniciar y hasta que regristro va a limitar
        
        //$query =mysqli_query($conexion,"SELECT idDonatario, Nombre, Contacto,Direccion,Telefono, Email,FechaAlta FROM Donatario WHERE Estatus = 1 LIMIT $desde,$por_pagina");

        $query = mysqli_query($conexion,"SELECT e.idEntrada,l.Titulo,.l.Editorial, e.Recibe,e.Adquisicion, p.Nombre, DATE_FORMAT(e.Fecha,'%d/%m/%Y') AS Fecha,e.Cantidad FROM Entrada e INNER JOIN Libro l on e.idLibro = l.idLibro INNER JOIN Proveedor p ON l.idProveedor = p.idProveedor ORDER BY e.idEntrada DESC LIMIT $desde,$por_pagina");



    //mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            ?>
                <tr id ="row_<?php echo $data['idEntrada']  ;?> "  >
                    <td><?php echo $data['idEntrada']   ;?></td>
                    <td><?php echo $data['Titulo'];  ?></td>
                    <td><?php echo $data['Editorial']   ;?></td>
                    <td><?php echo $data['Recibe']; ?></td>
                    <td><?php echo $data['Adquisicion']   ;?></td>
                    <td><?php echo $data['Nombre']   ;?></td>
                    <td><?php echo $data['Fecha']   ;?></td>
                    <td class="textright"><?php echo $data['Cantidad']   ;?></td>
                    <!-- <td><?php //echo $data['FechaAlta']   ;?></td> -->
                    <?php //if ($_SESSION['rol']==1 || $_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
                  <!--   <td>
                        <div class="div_acciones">
                            <div>
                                <button class="btn_view view_donacion" id="view_donacion" type="button" cl="<?php
                                //echo $data['idDonatario'] ;?>" f="<?//php// echo $data['idSalida']  ;?>">Ver</button>
                            </div>
                        </div> -->
                            <?php //}?>
                </tr>
       <?php     
        }


    }


    ?>

    </table>
    <div class="paginador">
        <ul>
            <?php
                if($pagina !=1)
                {
            ?>
            <li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
            <li><a href="?pagina=<?php echo $pagina -1;?>"><<<</a></li>
            <?php
            }
            ?>
            <?php
            for ($i=1; $i <= $total_paginas; $i++) { 
                if($i == $pagina){
                    echo '<li class="pageselected">'.$i.'</li>';
                }else{
                    echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
                }
            }
            ?>
            <?php
            if($pagina !=$total_paginas){
            ?>
            
            <li><a href="?pagina=<?php echo $pagina + 1 ;?>">>>></a></li>
            <li><a href="?pagina=<?php echo $total_paginas; ;?>">>|</a></li>
            <?php
            } 
            ?>

        </ul>
    </div>

</section>
    
</body>
<?php include "vistas/scripts.php" ;?>
<?php require_once "vistas/footer.php" ?>
</html>