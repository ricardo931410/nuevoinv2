<?php require_once "vistas/header.php";?>
<?php
session_start();
if ($_SESSION['rol'] !=1 ) {
    header("location: ../");
}
include '../conexion.php';
    if(!empty($_POST))
    {
        $alert2 ='';
        if (empty($_POST['nombre'])||empty($_POST['apellido'])||empty($_POST['email'])||empty($_POST['rol'])) {
            $alert2 = '<p class = "msg_error">Todos los campos son Obligatorios</p>';
        }else{
            $idusuario =$_POST['idusuario'];
            $nombre = $_POST['nombre'];
            $apellido = $_POST['apellido'];
            $email = $_POST['email'];
            $clave = $_POST['clave'];
            $estatus = $_POST['estatus'];
            $rol = $_POST['rol'];

            $query = mysqli_query($conexion,"SELECT * FROM Usuario 
                                            WHERE (Email = '$email' AND idUsuario != $idusuario)");

            
            $result = mysqli_fetch_array($query);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El Usuario ya existe</p>';
            }else{
                if (empty($_POST['clave'])) {
                $sql_update = mysqli_query($conexion, "UPDATE Usuario
                                                        SET Nombre = '$nombre', Apellido = '$apellido', Email ='$email', Estatus = '$estatus', idRol = '$rol' WHERE idUsuario = $idusuario" );

                }else{
                    $sql_update = mysqli_query($conexion, "UPDATE Usuario
                                                        SET Nombre = '$nombre', Apellido = '$apellido', Email ='$email', Estatus = '$estatus', Clave ='$clave', idRol = '$rol' WHERE idUsuario = $idusuario" );

                }
                if ($sql_update) {
                    $alert2 ='<p class = "msg_save">Usuario actualizado correctamente</p>';
                    header('Location: ListaUsuarios.php');
                }else{
                    $alert2 ='<p class = "msg_error">Error al actualizar el Usuario</p>';
                }
            }
        }
    }

    //Mostrar datos

    if(empty($_REQUEST['id'])) 
    {
        header('Location: ListaUsuarios.php');
        mysqli_close($conexion);
    }
    $iduser = mysqli_real_escape_string($conexion,$_REQUEST['id']);
    //$iduser= $_REQUEST['id'];

        $sql = mysqli_query($conexion,"SELECT u.idUsuario, u.Nombre, u.Apellido, u.Email, u.Estatus, r.idRol, r.Rol FROM Usuario u INNER JOIN Rol r on u.idRol = r.idRol WHERE idUsuario  = $iduser");

        mysqli_close($conexion);

        $result_sql= mysqli_num_rows($sql);
        if ($result_sql == 0) {
            header('Location: ListaUsuarios.php');
            
        }else{
            $option ='';
            while($data = mysqli_fetch_array($sql)){
                $iduser = $data['idUsuario'];
                $nombre = $data['Nombre'];
                $apellido = $data['Apellido'];
                $email = $data['Email'];
                $estatus = $data['Estatus'];
                $idrol = $data['idRol'];
                $rol = $data['Rol'];

                if ($idrol == 1) { 
                        $option = '<option value="'.$idrol.'" >'.$rol.'</option>';
                }else if ($idrol == 2) {
                        $option = '<option value="'.$idrol.'" >'.$rol.'</option>';
                }else if ($idrol == 3) {
                        $option = '<option value="'.$idrol.'" >'.$rol.'</option>';
                }else if ($idrol == 4) {
                    $option = '<option value="'.$idrol.'" >'.$rol.'</option>';
                }else if ($idrol == 5) {
                    $option = '<option value="'.$idrol.'" >'.$rol.'</option>';
                }
            }
        }

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar usuario</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>


<section id= "contenedor">

    <div class="form_register">
        <h1>Actualizar Usuario</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post">
        <input type="hidden" name="idusuario" value="<?php echo $iduser  ;?>">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre" value = "<?php echo $nombre; ?> ">
        <label for="apellido">Apellido</label>
        <input type="text" name ="apellido" id="apellido" placeholder="Apellido" value = "<?php echo $apellido; ?> ">
        <label for="email">Correo electronico</label>
        <input type="email" name="email" id="email" placeholder="Correo electronico" value = "<?php echo $email; ?> ">
        <label for="clave">Contraseña</label>
        <input type="password" name="clave" id="clave" placeholder="Contraseña">
        <label for="estatus">Estatus</label>
        <input type="text" name="estatus" id="estatus" placeholder="Estatus" value = "<?php echo $estatus; ?> ">
        <label for="rol">Rol de usuario</label>
        <?php
            include '../conexion.php';
            $query_rol = mysqli_query($conexion,"SELECT * FROM Rol");
            mysqli_close($conexion);
            $result_rol = mysqli_num_rows($query_rol);

           
        ?>

        <select name="rol" id="rol" class="NotItemOne">
            <?php
                echo $option;
                if ($result_rol > 0 ) {
                    while( $rol = mysqli_fetch_array($query_rol)){
            ?>
                <option value="<?php echo $rol['idRol'];  ?>"><?php echo $rol['rol'] ;  ?> </option>
            <?php
                    }
                }
            ?>

        </select>
        <input type="submit" value="Actualizar usuario" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php"; ?>
</html>

