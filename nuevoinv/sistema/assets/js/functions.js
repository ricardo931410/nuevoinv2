
$(document).ready(function(){

    //--------------------- SELECCIONAR FOTO libro ---------------------
    $("#foto").on("change",function(){

        var uploadFoto = document.getElementById("foto").value;
        var foto       = document.getElementById("foto").files;
        var nav = window.URL || window.webkitURL;
        var contactAlert = document.getElementById('form_alert');
        
            if(uploadFoto !='')
            {
                var type = foto[0].type;
                var name = foto[0].name;
                if(type != 'image/jpeg' && type != 'image/jpg' && type != 'image/png')
                {
                    contactAlert.innerHTML = '<p class="errorArchivo">El archivo no es v�lido.</p>';                        
                    $("#img").remove();
                    $(".delPhoto").addClass('notBlock');
                    $('#foto').val('');
                    return false;
                }else{  
                        contactAlert.innerHTML='';
                        $("#img").remove();
                        $(".delPhoto").removeClass('notBlock');
                        var objeto_url = nav.createObjectURL(this.files[0]);
                        $(".prevPhoto").append("<img id='img' src="+objeto_url+">");
                        $(".upimg label").remove();
                        
                    }
            }else{
                alert("No selecciono foto");
                $("#img").remove();
            }              
    });

    $('.delPhoto').click(function(){
    	$('#foto').val('');
    	$(".delPhoto").addClass('notBlock');
    	$("#img").remove();

        if ($("#foto_actual") && $("#foto_remove")) {
            $("#foto_remove").val('img_portada.png');
        }

    });
//////////////MODAL for add libro/////////////
$('.add_libro').click(function(e){
    e.preventDefault();
    var id = $(this).attr('libr');
    var action = 'info';
    //alert(libro);
    
    $.ajax({
        url: 'ajax.php',
        type: 'POST',
        async: true,
        data:{
            action:action,
            id:id
            },

        success: function (response) {
            if (response !='error') {
                var info = JSON.parse(response);
                
                //$('#libro_id').val(info.idLibro);
                //$('.namelibro').html(info.Titulo);
                                $('.bodymodal').html('<form action="" method="post" name="form_add_libro" id="form_add_libro" onsubmit="event.preventDefault(); sendDataLibro();">'+
                                '<h1>Agregar libros</h1>'+
                                '<h2 class="namelibro">'+info.Titulo+'</h2><br>'+
                                '<input type="number" name="cantidad" id ="txtCantidad" placeholder="Cantidad de libros nuevos" required><br>'+
                                '<!-- <input type="text" name="nombre" id ="nombre" placeholder="recibio" required> -->'+
                                '<input type="hidden" name="libro_id" id ="libro_id" value="'+info.idLibro+'" required>'+
                                '<input type="hidden" name="action" value ="addlibro" required>'+
                                '<div class="alert2 alertAddLibro"></div>'+
                                '<button type="submit" class="btn_new2">Agregar</button>'+
                                '<a href="#" class=" btn_cancel closemodal" onclick="closemodal();">Cerrar</a>'+
                        '</form>');
            }
        },
        error: function (response){
            console.log (error);
        }
    });


    //alert(libro);
    $('.modal').fadeIn();

});

//buscar donatario

$('#id_cliente').keyup(function (e) { 
    e.preventDefault();
    var dn = $(this).val();
    var action = 'searchCliente';

    $.ajax({
        url: "ajax.php",
        type: "POST",
        async: true,
        data : {action:action,cliente:dn},

        success: function (response) {
            //console.log(response)
            if (response == 0) {
                $('#idcliente').val('');
                $('#nom_cliente').val('');
                $('#cont_cliente').val('');
                $('#dir_cliente').val('');
                $('#tel_cliente').val('');
                $('#motivo').val('');
            }else{
                var data = $.parseJSON(response) 
                $('#idcliente').val(data.idDonatario);
                $('#nom_cliente').val(data.Nombre);
                $('#cont_cliente').val(data.Contacto);
                $('#dir_cliente').val(data.Direccion);
                $('#tel_cliente').val(data.Telefono);
                $('#motivo').val(data.Motivo);

                //bloque campos
                $('#nom_cliente').attr('disabled','disabled');
                $('#cont_cliente').attr('disabled','disabled');
                $('#dir_cliente').attr('disabled','disabled');
                $('#tel_cliente').attr('disabled','disabled');
                $('#motivo').attr('disabled','disabled');
            }
        },
        error: function (error) {
            
        }
        
        
    });
});

    

    //


    ///Buscar libro

    $('#txt_idlibro').keyup(function(e){
        e.preventDefault();

        var libro = $(this).val();
        var action = 'infolibro';

        if (libro != '') {
            
            $.ajax({
                url: "ajax.php",
                type: "POST",
                async: true,
                data: {action:action,libro:libro},

                success: function (response) {
                    //console.log(response);
                    if (response != 'error') 
                    {
                        //console.log(response);
                        var info = JSON.parse(response);
                        $('#txt_titulo').html(info.Titulo);
                        $('#txt_coleccion').html(info.Coleccion);
                        $('#txt_existencia').html(info.Ejemplares);
                        $('#txt_cant_libro').val('1');
                        //activar Cantidad
                        $('#txt_cant_libro').removeAttr('disabled');
                        //mostrar boton agregar
                        $('#add_product_venta').slideDown();
                    }else{
                        $('#txt_titulo').html('-');
                        $('#txt_coleccion').html('-');
                        $('#txt_existencia').html('-');
                        $('#txt_cant_libro').val('0');

                        //bloquear  cantidad
                        $('#txt_cant_libro').attr('disabled','disabled');
                        //ocultar boton agregar
                        $('#add_product_venta').slideUp();
                    }
    
            }, 
            error:function(error){
    
            }
            });
        }



    });

    //Validar cantidad de libros antes de agregar

    $('#txt_cant_libro').keyup(function (e) { 
        e.preventDefault();
        //validacion si en el campo cantidad sea mayor a uno y que sea menor al stick actual
        var existencia = parseInt ($('#txt_existencia').html());
        if (  ($(this).val() < 1 || isNaN($(this).val())) || ($(this).val() > existencia)) {
            $('#add_product_venta').slideUp();
        }else{
            $('#add_product_venta').slideDown();
        }
        
    });


    //Agregar  productos al detalle
    $('#add_product_venta').click(function (e) { 
        e.preventDefault();
        
        if ($('#txt_cant_libro').val()> 0) {
             var idlibro =  $('#txt_idlibro').val();
             var cantidad = $('#txt_cant_libro').val();
             var action =  'addLibroDetalle'

             $.ajax({
                 url: 'ajax.php',
                 type: "POST",
                 async: true,
                 data: {action:action,idlibro:idlibro,cantidad:cantidad},

                 success: function (response) {
                     
                    if (response != 'error') {

                        var info = JSON.parse(response);
                        $('#detalle_venta').html(info.detalle);
                        $('#detalle_totales').html(info.totales);

                        $('#txt_idlibro').val('');
                        $('#txt_titulo').html('-');
                        $('#txt_coleccion').html('-');
                        $('#txt_existencia').html('-');
                        $('#txt_cant_libro').val('');

                        //bloquear cantidad
                        $('#txt_cant_libro').attr('disabled','disabled');

                        //ocultar boton agregar

                        $('#add_product_venta').slideUp();
                        
                    }else{
                        console.log('Sin datos')

                    }
                    viewDonacion();
                 },
                 error : function (error) {
                     
                 }
             });
        }
    });


    //funcion del boton anular donacion
    $('#btn_anular_venta').click(function (e) { 
        e.preventDefault();

        var rows = $('#detalle_venta tr').length;

        if (rows > 0) {
            var action = 'anularDonacion';

            $.ajax({
                url: 'ajax.php',
                type: 'POST',
                async: true,
                data: {action:action},
                success: function (response) {
                    
                    if (response != 'error') {
                        location.reload();
                    }
                },
                error: function(error){

                }
            });

        }
        
    });

    //crear donacion
    $('#btn_hacer_donacion').click(function (e) { 
        e.preventDefault();

        var rows = $('#detalle_venta tr').length;

        if (rows > 0) {
            var action = 'crearDonacion';
            var codcliente = $('#idcliente').val();

            $.ajax({
                url: 'ajax.php',
                type: 'POST',
                async: true,
                data: {action:action,codcliente:codcliente},
                success: function (response) {
                    
                    if (response != 'error') {

                        var info = JSON.parse(response);
                        console.log(info);
                        generarPDF(info.idDonatario,info.idSalida);

                        location.reload();
                    }else{
                        console.log('no datos');
                    }
                },
                error: function(error){

                }
            });

        }
        
    });

    //ver donacion

    $('.view_donacion').click(function(e) { 
        e.preventDefault();
        var codCliente = $(this).attr('cl');
        var idsalida = $(this).attr('f');
        generarPDF(codCliente,idsalida);
        
    });
    
    /* $(function(){
        $('#bd-desde').on('change',function(){
            var desde = $('#bd-desde').val();
            var hasta = $('#bd-hasta').val();
    
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: 'desde='+desde+'&hasta='+hasta,
                success: function (datos) {
                    $('#agregar-registros').html(datos);
                }
            });
            return false;
    
        });
    
        $('#bd-hasta').on('change',function(){
            var desde = $('#bd-desde').val();
            var hasta = $('#bd-hasta').val();
    
            $.ajax({
                type: "POST",
                url: "ajax.php",
                data: 'desde='+desde+'&hasta='+hasta,
                success: function (datos) {
                    $('#agrega-registros').html(datos);
                }
            });
            return false;
    
        });
    });
     */
    
}); //end ready







function generarPDF(cliente,factura){
    var ancho = 1000;
    var alto = 1000;
    //calcular posicion x,y para centrar la venatana
    
    var x = parseInt((window.screen.width/2) - (ancho / 2));
    var y = parseInt((window.screen.height/2) - (alto / 2));
    
    $url = 'donaciones/generarDonacion.php?cl='+cliente+'&f='+factura;
    window.open($url,"Factura","left="+x+",top="+y+",height="+alto+",width="+ancho+",scrollbar=si,location=no,resizable=si,menubar=no");
}



//eliminar del detalle temporal
function del_libro_detalle(idDetalleTempSalida){
    
    var action = 'delLibroDetalle';
    var id_detalle = idDetalleTempSalida;
    
    $.ajax({
        url: 'ajax.php',
        type: "POST",
        async: true,
        data: {action:action,id_detalle:id_detalle},
        
        success: function (response) {
            
            if (response != 'error') {
                
                var info = JSON.parse(response);
                
                $('#detalle_venta').html(info.detalle);
                $('#detalle_totales').html(info.totales);
                
                $('#txt_idlibro').val('');
                $('#txt_titulo').html('-');
                $('#txt_coleccion').html('-');
                $('#txt_existencia').html('-');
                $('#txt_cant_libro').val('');
                
                //bloquear cantidad
                $('#txt_cant_libro').attr('disabled','disabled');
                
                //ocultar boton agregar
                
                $('#add_product_venta').slideUp();
                
                
                
            }else{
                $('#detalle_venta').html('');
                $('#detalle_totales').html('');
            }
            viewDonacion();
        },
        error : function (error) {
            
        }
    });
    
    
}


//mostra/ocultar boton de donacion

function viewDonacion(){
    if ($('#detalle_venta tr ').length > 0) 
    {
        $('#btn_hacer_donacion').show();
    }else{
        $('#btn_hacer_donacion').hide();
    }
}


function serchForDetalle(idusuario) {
    var action = 'serchForDetalle';
    var user = idusuario;
    
    $.ajax({
        url: 'ajax.php',
        type: "POST",
        async: true,
        data: {action:action,user:user},
        
        success: function (response) {
            
            if (response != 'error') {
                
                var info = JSON.parse(response);
                $('#detalle_venta').html(info.detalle);
                $('#detalle_totales').html(info.totales);
                
                
            }else{
                console.log('Sin datos')
                
            }
            viewDonacion();
        },
        error : function (error) {
            
        }
    });
}





function sendDataLibro(){
    //alert("enviar datos");
    
    $('.alertAddLibro').html('');
    
    $.ajax({
        url: 'ajax.php',
        type: 'POST',
        async: true,
        data: $('#form_add_libro').serialize(),
        
        success: function (response) {
            if (response == 'error') 
            {
                $('.alertAddLibro').html('<p style="color:red;">Error al Agregar libro </p>');
            }else{
                var info = JSON.parse(response);
                $('.row'+info.libro_id+' .celEjem').html(info.nueva_existencia);
                $('#txtCantidad').val('');
                $('.alertAddLibro').html('<p>Libros Agregados correctamente</p>');
                $('.modal').fadeOut();
            }
        },
        error: function (response){
            console.log (error);
        }
    });
}

function closemodal(){
    $('.alertAddLibro').html('');
    $('#txtCantidad').val('');
    $('#nombre').val('');
    $('.modal').fadeOut();
}