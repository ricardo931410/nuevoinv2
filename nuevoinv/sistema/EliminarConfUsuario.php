<?php
session_start();
if ($_SESSION['rol'] !=1 ) {
    header("location: ../");
}
include "../conexion.php";

if (!empty($_POST)) 
{
    if ($_POST['idusuario']== 1) {
        header ("location: ListaUsuarios.php");
        mysqli_close($conexion);
        exit;
    }
    $idusuario= $_POST['idusuario'];
    //$query_delete = mysqli_query(conexion,"DELETE FROM Usuario WHERE  idUsuario= $idusuario");
    $query_delete = mysqli_query($conexion,"UPDATE Usuario SET Estatus= 0  WHERE idUsuario = $idusuario");
    mysqli_close($conexion);

    if ($query_delete) {
        header("location: ListaUsuarios.php");
    }else{
        echo "Error al eliminar";
    }
}


if (empty($_REQUEST['id'])|| $_REQUEST['id'] == 1) {
    header("location: ListaUsuarios.php");
    mysqli_close($conexion);
}else{
    
    $idusuario = $_REQUEST['id'];

    $query = mysqli_query($conexion,"SELECT u.Nombre,u.Apellido,u.Email,r.Rol FROM Usuario u INNER JOIN Rol r on u.idRol = r.idRol where u.idUsuario = $idusuario");
    mysqli_close($conexion);
    $result = mysqli_num_rows($query);
    if ($result>0) {
        while($data = mysqli_fetch_array($query)){
            $nombre = $data['Nombre'];
            $apellido = $data['Apellido'];
            $email = $data['Email'];
            $rol = $data['Rol'];
        }
    }else{
        header ("location: ListaUsuarios.php");
    }


}
?>


<?php require_once "vistas/header.php";?>


<section id="contenedor">
   <!--  <h1>Eliminar Usuario</h1> -->
    <div class="data_delete">
        <h2>¿Esta seguro de eliminar el siguiente usuario?</h2>
        <p>Nombre: <span><?php echo $nombre ;?>  </span></p>
        <p>Apellido: <span> <?php echo $apellido  ;?> </span></p>
        <p>Tipo de Usuario: <span> <?php echo $rol  ;?> </span></p>

        <form action="" method="post">
            <input type="hidden" name="idusuario" value="<?php echo $idusuario ;?>">
            <a href="ListaUsuarios.php" class="btn_cancel">Cancelar</a>
            <input type="submit" value="Aceptar" class="btn_ok">


        </form>
    </div>

</section>

<?php require_once "vistas/footer.php" ?>