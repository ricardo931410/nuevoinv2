<?php require_once "vistas/header.php";

if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2 and $_SESSION['rol'] !=3) {
    header("location: ../");
    //header('Location: ListaDonatarios.php');

}

//session_start();

include '../conexion.php';
    if(!empty($_POST))
    {
        $alert2 ='';
        if (empty($_POST['nombre'])||empty($_POST['contacto'])||empty($_POST['direccion'])||empty($_POST['telefono'])||empty($_POST['email'])) {
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            $iddonatario =$_POST['iddonatario'];
            $nombre = $_POST['nombre'];
            $contacto = $_POST['contacto'];
            $direccion = $_POST['direccion'];
            $telefono = ($_POST['telefono']);
            $email = $_POST['email'];
            $motivo = $_POST['motivo'];
            $estatus = $_POST['estatus'];

            $query = mysqli_query($conexion,"SELECT * FROM Donatario 
                                            WHERE (Nombre = '$nombre' AND idDonatario != $iddonatario)");
            $result = mysqli_fetch_array($query);
            //$result = count($result);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El Donatario ya existe</p>';
            }else{
                    if (empty($_POST['estatus'])) {
                        $sql_update = mysqli_query($conexion, "UPDATE Donatario
                                                        SET Nombre = '$nombre', Contacto = '$contacto', Direccion ='$direccion', Telefono ='$telefono', Email = '$email', Motivo = '$motivo'
                                                        WHERE idDonatario= $iddonatario");
                    }else{
                        $sql_update = mysqli_query($conexion, "UPDATE Donatario
                                                        SET Nombre = '$nombre', Contacto = '$contacto', Direccion ='$direccion', Telefono ='$telefono', Email = '$email', Motivo = '$motivo', Estatus = '$estatus'
                                                        WHERE idDonatario= $iddonatario");

                    }
                }
                if ($sql_update) {
                    $alert2 ='<p class = "msg_save">Usuario actualizado correctamente</p>';
                    header('location: ListaDonatarios.php');
                }else{
                    $alert2 ='<p class = "msg_error">Error al actualizar el donatario</p>';
                }
            }
        }

    //Mostrar datos

    
        if(empty($_REQUEST['id'])) 
    {
        header('Location: ListaDonatarios.php');
        //mysqli_close($conexion);
    }
    if (!is_numeric($_REQUEST['id'])){
        header('Location: ListaDonatarios.php');
        mysqli_close($conexion);
    }
            $iddonatario = mysqli_real_escape_string($conexion,$_REQUEST['id']);
            //$iddonatario = $_REQUEST['id'];
                if ($_SESSION['rol']==1) {
                    $sql=mysqli_query($conexion,"SELECT idDonatario, Nombre,Contacto,Direccion,Telefono,Email,Motivo,Estatus From Donatario WHERE idDonatario = $iddonatario");
            
                }else{
                    $sql=mysqli_query($conexion,"SELECT idDonatario, Nombre,Contacto,Direccion,Telefono,Email, Motivo, Estatus From Donatario WHERE idDonatario = $iddonatario and Estatus = 1");
                }

                mysqli_close($conexion);

                $result_sql= mysqli_num_rows($sql);
                if ($result_sql == 0) {
                        header('Location: ListaDonatarios.php');
        
                    }else{
                        while($data = mysqli_fetch_array($sql)){
                            $iddonatario = $data['idDonatario'];
                            $nombre = $data['Nombre'];
                            $contacto = $data['Contacto'];
                            $direccion = $data['Direccion'];
                            $telefono = $data['Telefono'];
                            $email = $data['Email'];
                            $motivo = $data['Motivo'];
                            $estatus = $data['Estatus'];
                    }
                }

?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Editar donatario</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>
<?php //require_once "vistas/header.php";?>
<?php
//if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2 and $_SESSION['rol'] !=3) {
 //   header("location: ../");
//}
?>
<section id= "contenedor">

    <div class="form_register">
        <h1>Actualizar Donatario</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post">
        <input type="hidden" name="iddonatario" value="<?php echo $iddonatario  ;?>">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre" value = "<?php echo $nombre; ?> ">
        <label for="contacto">Contacto</label>
        <input type="text" name ="contacto" id="contacto" placeholder="Contacto" value = "<?php echo $contacto; ?> ">
        <label for="direccion">Dirección</label>
        <input type="texto" name="direccion" id="direccion" placeholder="Dirección" value = "<?php echo $direccion; ?> ">
        <label for="telefono">Telefono</label>
        <input type="text" name="telefono" id="telefono" placeholder="telefono" value = "<?php echo $telefono; ?> ">
        <label for="email">Correo electronico</label>
        <input type="texto" name="email" id="email" placeholder="email" value = "<?php echo $email; ?> ">
        <label for="motivo">Motivo</label>
        <input type="texto" name="motivo" id="motivo" placeholder="motivo" value = "<?php echo $motivo; ?> ">

        <?php if ($_SESSION['rol']==1)
        {?>
        <label for="estatus">Estatus</label>
        <input type="text" name="estatus" id="estatus" placeholder="estatus" value = "<?php echo $estatus;?> ">
        <?php }?>   

        </select>
        <input type="submit" value="Actualizar donatario" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php"; ?>
</html>
