<?php
//session_start();
//if ($_SESSION['rol'] !=1 ) {
//    header("location: ../");
//}
include "../conexion.php";
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "vistas/scripts.php" ;?>
    <title>Lista de libros</title>
</head>
<body>
<?php require_once "vistas/header.php";?>


<section id="contenedor">
    <h1>Lista de libros</h1>
    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
    <a href="RegistroLibro.php" class="btn_new">Registrar nuevo libro</a>
   <!--  <form action="ReporteLibros.php" method="post" class="form_buscar">
       
        <input type="submit" value="Reporte PDF" class="btn_buscar" name="" id="">
    </form> -->
    <!-- <form action="ReporteLibros.php" method="post" class="form_buscar">
        <input type="submit" value="Generar reporte" class="btn_buscar" name="" id="">
    </form> -->

    <?php }?>
    <form action="BuscarLibro.php" method="post" class="form_buscar">
        <input type="text" name="busqueda" id="busqueda" placeholder="Buscar">
        <input type="submit" value="Buscar" class="btn_buscar" name="" id="">
    </form>

    <table>
        <tr>
            <th>ID</th>
            <th>Portada</th>
            <th>Título</th>
            <th>Autor</th>
            <th>Ilustrador</th>
            <th>Editorial</th>
            <th>Colección</th>
            <th>Año</th>
            <th>Páginas</th>
            <th>Formato</th>
            <th>Existencias</th>
            <th>Adquisición</th>
            <?php if ($_SESSION['rol']==1||$_SESSION['rol']==3 || $_SESSION['rol']==2) {?>
            <th>Recibió</th>
            <?php }?>
            <!-- <th>Fecha de registro</th> -->
            <?php if ($_SESSION['rol']==3 || $_SESSION['rol']==2) {?>
            <th>Acción</th>
            <?php }?>
            <?php if ($_SESSION['rol']==1) {?>
            <th>Acciones</th>
            <?php }?>
        </tr>
    <?php
    
    //paginador
    //cuantos registros estan activos
    $sql_registe =mysqli_query($conexion, "SELECT COUNT(*) AS total_registro FROM Libro");

    //guarda el resultado en un array
    $result_register = mysqli_fetch_array($sql_registe);
    //guarda en una variable el numero total de registros
    $total_registro = $result_register['total_registro'];

    //variable que contiene el  numero de registros por pagina
    $por_pagina = 80;

    //validacion de el paginador manda por el url
    if(empty($_GET['pagina']))
    {
        $pagina =1;
    }else{
        $pagina = $_GET['pagina'];
    }

    $desde = ($pagina-1) * $por_pagina;
    $total_paginas = ceil($total_registro / $por_pagina); 

    //el query retorna la informacion el Limit indica desde donde va a iniciar y hasta que regristro va a limitar
    if (empty($_SESSION['rol']==1)) {
        $query =mysqli_query($conexion,"SELECT idLibro,Portada,Titulo,Autor,Ilustrador,Editorial,Coleccion,Año,Paginas,Formato,Ejemplares,Adquisicion,Recibe FROM Libro WHERE Estatus = 1 ORDER BY idLibro DESC LIMIT $desde,$por_pagina");
    
    }else{
        $query =mysqli_query($conexion,"SELECT idLibro,Portada,Titulo,Autor,Ilustrador,Editorial,Coleccion,Año,Paginas,Formato,Ejemplares,Adquisicion,Recibe FROM Libro ORDER BY idLibro DESC LIMIT $desde,$por_pagina");
    }


    mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            if($data['Portada'] != 'img_portada.png'){
                $foto = 'portadas/'.$data['Portada'];

            }else{
                $foto = 'portadas/'.$data['Portada'];
            }
            ?>
                <tr class="row<?php echo $data['idLibro']   ;?>">
                    <td><?php echo $data['idLibro']   ;?></td>
                    <td class="img_portada"><img src="<?php echo $foto ;?>" alt="<?php echo $data['Titulo'];  ?>"></td>
                    <td><?php echo $data['Titulo'];  ?></td>
                    <td><?php echo $data['Autor']   ;?></td>
                    <td><?php echo $data['Ilustrador']; ?></td>
                    <td><?php echo $data['Editorial']   ;?></td>
                    <td><?php echo $data['Coleccion']   ;?></td>
                    <td><?php echo $data['Año']   ;?></td>
                    <td><?php echo $data['Paginas']   ;?></td>
                    <td><?php echo $data['Formato']   ;?></td>
                    <td class="celEjem"><?php echo $data['Ejemplares']   ;?></td>
                    <td><?php echo $data['Adquisicion']   ;?></td>
                    <?php if ($_SESSION['rol']==1||$_SESSION['rol']==2 || $_SESSION['rol']==3){?>
                    <td><?php echo $data['Recibe']   ;?></td>
                    <?php }?>
                    <!-- <td><?php //echo $data['FechaAlta']   ;?></td> -->
                    <?php if ($_SESSION['rol']==1){?>
                    <td>
                        <a class="link_add add_libro" libr ="<?php echo $data['idLibro'];?>" href="#">Agregar</a>
                        |
                        <?php
                    } 
                    ?>
                   
                    <?php if ($_SESSION['rol']==1){?>
                    
                        <a href="EditarLibro.php?id=<?php echo $data['idLibro'];?>" class="link_edit">Editar</a>
                        
                    </td>
                        <?php }?>

                    <?php if ($_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
                    <td>
                    
                        <a href="EditarLibro.php?id=<?php echo $data['idLibro'];?>" class="link_edit">Editar</a>
                        
                        <?php }?>
                        
                    </td>
                    

                        <!-- 
                                |

                                    <a href="EliminarConfDonatario.php?id=<?php //echo $data['idDonatario'];?>" class="link_delete">Eliminar</a>
                            -->
                </tr>
       <?php     
        }


    }


    ?>

    </table>
    <div class="paginador">
        <ul>
            <?php
                if($pagina !=1)
                {
            ?>
            <li><a href="?pagina=<?php echo 1; ?>">|<</a></li>
            <li><a href="?pagina=<?php echo $pagina -1;?>"><<<</a></li>
            <?php
            }
            ?>
            <?php
            for ($i=1; $i <= $total_paginas; $i++) { 
                if($i == $pagina){
                    echo '<li class="pageselected">'.$i.'</li>';
                }else{
                    echo '<li><a href="?pagina='.$i.'">'.$i.'</a></li>';
                }
            }
            ?>
            <?php
            if($pagina !=$total_paginas){
            ?>
            
            <li><a href="?pagina=<?php echo $pagina + 1 ;?>">>>></a></li>
            <li><a href="?pagina=<?php echo $total_paginas; ;?>">>|</a></li>
            <?php
            } 
            ?>

        </ul>
    </div>

</section>
    
</body>
<?php require_once "vistas/footer.php" ?>
</html>
