<?php require_once "vistas/header.php";

//echo md5($_SESSION['idUsuario']);
?>
<?php
//session_start();
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2) {
    header("location: ../");
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <?php include "vistas/scripts.php";?>
    <title>Nueva donación</title>
</head>
<body>

<section id= "contenedor">
    <div class="title_page">
        <h1>Nueva donación</h1>
    </div>
    <div class="datos_cliente">
        <div class="action_cliente">
            <h4>Datos del donatario</h4>
           
        </div>
        <form action="" name="form_new_cliente_venta" id="form_new_cliente_venta" class="datos" >
        <input type="hidden" name="action" id="" value="addCliente">
        <input type="hidden" id="idcliente" name="idcliente" values="" required>
        <div class="wd30">
            <label for="">ID donatario</label>
            <input type="text" name="id_cliente" id="id_cliente">
        </div>
        <div class="wd30">
            <label for="">Nombre</label>
            <input type="text" name="nom_cliente" id="nom_cliente" disabled required>
        </div>
        <div class="wd30">
            <label for="">Contacto</label>
            <input type="text" name="cont_cliente" id="cont_cliente" disabled required>
        </div>
        <div class="wd60">
            <label for="">Dirección</label></label>
            <input type="text" name="dir_cliente" id="dir_cliente" disabled required>
        </div>
        <div class="wd30">
            <label for="">Teléfono</label></label>
            <input type="text" name="tel_cliente" id="tel_cliente" disabled required>
        </div>
        <div class="wd100">
            <label for="">Motivo</label></label>
            <input type="text" name="motivo" id="motivo" disabled required>
        </div>
        
    </form>
    
</div>     


        <div class="datos_venta">
            <h4>Datos de la donación</h4>
                <div class="datos">
                    <div class="wd50">
                        <label for="">Autorizo</label>
                        <p><?php echo $_SESSION['nombre'], $_SESSION['apellido'] ;  ?></p>
                    </div>
                    <div class="wd50">
                            <label for="">Acciones</label>
                            <div id="acciones_venta">
                            <a href="#" class="btn_ok textcenter" id="btn_anular_venta">Anular</a>
                            <a href="#" class="btn_new textcenter" id="btn_hacer_donacion" style="display:none"; >Crear donación</a>
                     </div>
            </div>
        </div>
    </div>
    <table class="tbl_venta">
    <thead>
    <tr>
        <th width="40px">ID</th>
        <th>Título</th>
        <th>Colección</th>
        <th>Existencias</th>
        <th width="20px">Cantidad</th>
        <th>Acción</th>
    </tr>
    <tr>
        <td width="80px"><input type="text" name="txt_idlibro" id="txt_idlibro"></td>
        <td id="txt_titulo">--</td>
        <td id="txt_coleccion">--</td>
        <td id="txt_existencia">--</td>
        <td width="80px"><input type="text" name="txt_cant_libro" id="txt_cant_libro" value="0" min="1" disabled> </td>
        <td><a href="#" id="add_product_venta" class="link_add">Agregar</a> </td>
    </tr>
    <tr>
        <th>ID</th>
        <th colspan="2">Título</th>
        <th colspan="1">Colección</th>
        <th>Cantidad</th>
        <th>Acción</th>
    </tr>
    </thead>
    <tbody id="detalle_venta">
    <!-- Contenido AJAX -->
    </tbody>
    <tfoot id = "detalle_totales">
        <!-- Contenido AJAX -->
    </tfoot>
    </table>
</section>
<?php require_once "vistas/footer.php" ?>

<script type="text/javascript">
    $(document).ready(function(){
        var usuarioid = '<?php echo $_SESSION['idUsuario']; ?>';
        serchForDetalle(usuarioid);
    });

</script>
</body>


</html>