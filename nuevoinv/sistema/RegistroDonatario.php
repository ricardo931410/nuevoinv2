<?php require_once "vistas/header.php";?>
<?php
//session_start();
if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2) {
   header("location: ../");
}

include '../conexion.php';
    if(!empty($_POST))
    {
        $alert2 ='';
        if (empty($_POST['nombre'])|| empty($_POST['contacto'])) {
            $alert2 = '<p class = "msg_error">Todos los campos son obligatorios</p>';
        }else{
            
            $nombre = mysqli_real_escape_string($conexion,$_POST['nombre']);
            $contacto = mysqli_real_escape_string($conexion,$_POST['contacto']);
            $direccion = mysqli_real_escape_string($conexion,$_POST['direccion']);
            $telefono = mysqli_real_escape_string($conexion,$_POST['telefono']);
            $email = mysqli_real_escape_string($conexion,$_POST['email']);
            $motivo = mysqli_real_escape_string($conexion,$_POST['motivo']);

            $query = mysqli_query($conexion,"SELECT * FROM Donatario WHERE Motivo = '$motivo' AND Nombre ='$nombre' ");
            //mysqli_close($conexion);
            $result = mysqli_fetch_array($query);
            if ($result > 0) {
                $alert2 ='<p class = "msq_error">El Nombre o el motivo del donatario ya existe</p>';
            }else{
                $query_insert = mysqli_query($conexion,"INSERT INTO Donatario(Nombre,Contacto,Direccion,Telefono,Email,Motivo) 
                VALUES('$nombre','$contacto','$direccion','$telefono','$email','$motivo')");
                if ($query_insert) {
                    $alert2 ='<p class = "msg_save">Donatario registrado correctamente</p>';
                }else{
                    $alert2 ='<p class = "msg_error">Error al registrar el donatario</p>';
                }
            }
        }
        mysqli_close($conexion);
    }

?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Registro de donatario</title>
<?php include "vistas/scripts.php" ;?>
</head>
<body>

<?php
//if ($_SESSION['rol'] !=1 and $_SESSION['rol'] !=2) {
//    header("location: ../");
//}
?>
<section id= "contenedor">

    <div class="form_register">
        <h1>Registro de Donatario</h1>
        <hr>
    <div class = "alert2"> <?php echo isset($alert2) ? $alert2: ' '   ;?></div>

    <form action="" method="post">
        <label for="nombre">Nombre</label>
        <input type="text" name="nombre" id="nombre" placeholder="Nombre">
        <label for="contacto">Contacto</label>
        <input type="text" name ="contacto" id="contacto" placeholder="contacto">
        <label for="direccion">Dirección</label>
        <input type="text" name ="direccion" id="direccion" placeholder="Direccion">
        <label for="telefono">Teléfono</label>
        <input type="number" name ="telefono" id="telefono" placeholder="Telefono">
        <label for="email">Correo electrónico</label>
        <input type="email" name="email" id="email" placeholder="Correo Electronico">
        <label for="motivo">Motivo de donación</label>
        <input type="motivo" name="motivo" id="motivo" placeholder="Motivo de la donación">
        
        <input type="submit" value="Registrar Donatario" class="btn-save">

    </form>


</div>


</section>
    
</body>
<?php require_once "vistas/footer.php" ?>

</html>

