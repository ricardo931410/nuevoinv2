<?php
session_start();

include "../conexion.php";

            ////agrear libros
            if ($_POST) 
            {
                //print_r($_POST);
                //exit;

                //Extraer datos del libro
                if ($_POST['action']=='info') 
                {
                    $libro_id = intval($_POST['id']);
                    $sql = mysqli_query($conexion,"SELECT idLibro,Titulo FROM Libro WHERE idLibro = $libro_id");
                    mysqli_close($conexion);
                    $result = mysqli_num_rows($sql);
                    if ($result > 0) 
                    {
                        $data = mysqli_fetch_assoc($sql);
                        echo json_encode($data,JSON_UNESCAPED_UNICODE);
                        exit;
                    }
                    echo 'error';
                    exit;
                }


                //////agregar productos a entrada//////
                if ($_POST['action'] == 'addlibro')
                //print_r($_POST);
                //exit;
                {
                    if (!empty($_POST['cantidad']) || !empty($_POST['libro_id']))
                    {
                        $cantidad = $_POST['cantidad'];
                        $nombre = $_POST['nombre'];
                        $libro_id = $_POST['libro_id'];
                        $idusuario = $_SESSION['idUsuario'];

                        /* $query_insert = mysqli_query($conexion,"INSERT INTO Entrada(idLibro,idUsuario,Cantidad) 
                        VALUES($libro_id,$idusuario,$cantidad)"); */

                        //if ($query_insert)
                        //{
                            //ejeutar el procedimiento almacenado
                            $query_upd = mysqli_query($conexion,"CALL acualizarlibro($cantidad,$libro_id)");
                            $result_pro = mysqli_num_rows($query_upd);
                            if ($result_pro > 0) {
                                $data = mysqli_fetch_assoc($query_upd);
                                $data['libro_id']= $libro_id;
                                echo json_encode($data,JSON_UNESCAPED_UNICODE);
                                exit;
                            }else{
                                echo 'error';
                            }
                            mysqli_close($conexion);
                        //}else{
                        //    echo 'error insert ';
                      //  }
                    }else{
                        echo 'Error en validacion';
                        }
                        exit;
                }
            }


       // extraer datos del libro
       if($_POST['action'] == 'infolibro') {
           $libro_id = ($_POST['libro']);

           $query = mysqli_query($conexion,"SELECT * FROM Libro WHERE idLibro = $libro_id AND Estatus = 1");

           $result = mysqli_num_rows($query);

           if ($result > 0) {
               $data = mysqli_fetch_assoc($query);
               echo json_encode($data,JSON_UNESCAPED_UNICODE);
               exit;
           }
           echo 'error';
           exit;
       }



            //buscar donatario
            if ($_POST['action']=='searchCliente') {
               
                if (!empty($_POST['cliente'])) {
                    $iddona = $_POST['cliente'];

                    $query = mysqli_query($conexion,"SELECT * FROM Donatario WHERE idDonatario LIKE '$iddona'AND Estatus = 1");
                    $result = mysqli_num_rows($query);

                    $data ='';
                    if ($result > 0) {
                        $data = mysqli_fetch_assoc($query);

                    }else {
                        $data = 0;
                    }
                     echo json_encode($data,JSON_UNESCAPED_UNICODE);
                }
                exit;
            }

          


            //agregar libros al detalle temporal
            if ($_POST['action']=='addLibroDetalle'){
                //print_r($_POST);exit;
                if (empty($_POST['idlibro'])||empty($_POST['cantidad'])) {
                    echo 'error';
                }else {
                    $idlibro = $_POST['idlibro'];
                    $cantidad = $_POST['cantidad'];
                    $token = md5($_SESSION['idUsuario']);

                    $query_detalle_temp = mysqli_query($conexion,"CALL add_detalle_temp($idlibro,$cantidad,'$token')");
                    $result = mysqli_num_rows($query_detalle_temp);

                    $detalleTabla = '';
                    $TotalLibros = 0;
                    $arrayData = array();

                    if ($result > 0) {
                        
                        while ($data = mysqli_fetch_assoc($query_detalle_temp)) {
                            
                            $TotalLibros = $TotalLibros + $data['cantidad'];

                            $detalleTabla .= '<tr>
                                <td>'.$data['idLibro'].'</td>
                                <td colspan="2">'.$data['Titulo'].'</td>
                                <td colspan="1">'.$data['Coleccion'].'</td>
                                <td class="textcenter">'.$data['cantidad'].'</td>
                                <td class="">
                                    <a class="link_delete" href="" onclick="event.preventDefault(); del_libro_detalle('.$data['idDetalleTempSalida'].');">Eliminar</a>
                                </td>
                            </tr>';
                        }
                        $DetalleTotales = '<tr>
                                        <td colspan="4" class="text_right">Total libros:</td>
                                        <td class="text_right">'.$TotalLibros.'</td>
                                    </tr>';

                        $arrayData['detalle'] =  $detalleTabla;
                        $arrayData['totales'] = $DetalleTotales;

                        echo json_encode($arrayData,JSON_UNESCAPED_UNICODE);
                    }else {
                        echo 'error';
                    }
                    mysqli_close($conexion);

                }
                exit;
            }

            //Extrae datos del  detalle temp
            if ($_POST['action']=='serchForDetalle'){
                //print_r($_POST);exit;
                if (empty($_POST['user'])) {
                    echo 'error';
                }else {
                    
                    $token = md5($_SESSION['idUsuario']);
                    //$token = md5($_POST['user']);

                    $query = mysqli_query($conexion,"SELECT tmp.idDetalleTempSalida,
                                                            tmp.token_user,
                                                            tmp.cantidad,
                                                            l.idLibro,
                                                            l.Titulo,
                                                            l.Coleccion
                                                            FROM detalle_temp tmp 
                                                            INNER JOIN Libro l 
                                                            ON tmp.idLibro = l.idLibro
                                                            WHERE token_user = '$token' ");

                    $result = mysqli_num_rows($query);

                    $detalleTabla = '';
                    $TotalLibros = 0;
                    $arrayData = array();

                    if ($result > 0) {
                        
                        while ($data = mysqli_fetch_assoc($query)) {
                            
                            $TotalLibros = $TotalLibros + $data['cantidad'];

                            $detalleTabla .= '<tr>
                                <td>'.$data['idLibro'].'</td>
                                <td colspan="2">'.$data['Titulo'].'</td>
                                <td colspan="1">'.$data['Coleccion'].'</td>
                                <td class="textcenter">'.$data['cantidad'].'</td>
                                <td class="">
                                    <a class="link_delete" href="" onclick="event.preventDefault(); del_libro_detalle('.$data['idDetalleTempSalida'].');">Eliminar</a>
                                </td>
                            </tr>';
                        }
                        $DetalleTotales = '<tr>
                                        <td colspan="4" class="text_right">Total libros:</td>
                                        <td class="text_right">'.$TotalLibros.'</td>
                                    </tr>';

                        $arrayData['detalle'] =  $detalleTabla;
                        $arrayData['totales'] = $DetalleTotales;

                        echo json_encode($arrayData,JSON_UNESCAPED_UNICODE);
                    }else {
                        echo 'error';
                    }
                    mysqli_close($conexion);

                }
                exit;
            }

            //eliminar del detalle temporal
            if ($_POST['action']=='delLibroDetalle'){

                if (empty($_POST['id_detalle'])) {
                    echo 'error';
                }else {
                    $id_detalle = $_POST['id_detalle'];
                    $token = md5($_SESSION['idUsuario']);

                    $query_detalle_temp = mysqli_query($conexion,"CALL del_detalle_temp($id_detalle,'$token')");
                    $result = mysqli_num_rows($query_detalle_temp);

                    $detalleTabla = '';
                    $TotalLibros = 0;
                    $arrayData = array();

                    if ($result > 0) {
                        
                        while ($data = mysqli_fetch_assoc($query_detalle_temp)) {
                            
                            $TotalLibros = $TotalLibros + $data['cantidad'];

                            $detalleTabla .= '<tr>
                                <td>'.$data['idLibro'].'</td>
                                <td colspan="2">'.$data['Titulo'].'</td>
                                <td colspan="1">'.$data['Coleccion'].'</td>
                                <td class="textcenter">'.$data['cantidad'].'</td>
                                <td class="">
                                    <a class="link_delete" href="" onclick="event.preventDefault(); del_libro_detalle('.$data['idDetalleTempSalida'].');">Eliminar</a>
                                </td>
                            </tr>';
                        }
                        $DetalleTotales = '<tr>
                                        <td colspan="4" class="text_right">Total libros:</td>
                                        <td class="text_right">'.$TotalLibros.'</td>
                                    </tr>';

                        $arrayData['detalle'] =  $detalleTabla;
                        $arrayData['totales'] = $DetalleTotales;

                        echo json_encode($arrayData,JSON_UNESCAPED_UNICODE);
                    }else {
                        echo 'error';
                    }
                    mysqli_close($conexion);

                }
                exit;
            }

            //anular donacion
            
            if ($_POST['action'] == 'anularDonacion') {
                $token = md5($_SESSION['idUsuario']);

                $query_del = mysqli_query($conexion,"DELETE FROM detalle_temp WHERE token_user = '$token'");

                mysqli_close($conexion);

                if ($query_del) {
                    echo 'ok';
                }else {
                    echo 'error';
                }
                exit;
            }

            //procesar donacion
            
            if ($_POST['action'] == 'crearDonacion') {

                //print_r($_POST); exit;

                if (empty($_POST['codcliente']) ) {
                    echo 'error';
                }else {
                    $codcliente = $_POST['codcliente'];
                }

                $token =  md5($_SESSION['idUsuario']);
                $usuario =  $_SESSION['idUsuario'];

                $query = mysqli_query($conexion,"SELECT * FROM detalle_temp WHERE token_user = '$token'");
                $result = mysqli_num_rows($query);

                if ($result > 0) {
                    $query_procesar = mysqli_query($conexion,"CALL procesar_salida($usuario,$codcliente,'$token')");
                    $result_detalle = mysqli_num_rows($query_procesar);

                    if ($result_detalle > 0) {
                        //$query_motivo = mysqli_query($conexion,"INSERT INTO Salida Entrega,Motivo VALUE('$entrega','$motivo')");
                        $data = mysqli_fetch_assoc($query_procesar);
                        echo json_encode($data,JSON_UNESCAPED_UNICODE);
                    }else {
                        echo 'error';
                    }
                }else {
                    echo 'error';
                }
                mysqli_close($conexion);
                exit;

            }

            //busqueda para reportes

            



            /* //reportes en pdf
            $desde = $_POST['desde'];
            $hasta = $_POST['hasta'];
            //comprobar que las fechas existan 
            
            if(isset($desde)==false){
                $desde= $hasta;
            }
            if (isset($hasta)==false) {
                $hasta = $desde;
            }
            //ejequetamos consulta de busqueda
            
            $registro = mysqli_query("SELECT l.idLibro, l.Titulo, (s.idSalida) AS Vale, DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha ,d.Nombre,d.Motivo, ds.Cantidad,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo FROM DetalleSalida ds
            INNER JOIN Salida s
            ON ds.idSalida = s.idSalida
            INNER JOIN Libro l
            ON ds.idLibro = l.idLibro
            INNER JOIN Donatario d
            ON s.idDonatario = d.idDonatario
            INNER JOIN Usuario u 
            ON s.idUsuario = u.idUsuario
            WHERE s.Fecha BETWEEN '$desde AND '$hasta'
            ORDER by s.Fecha DESC");
            
            //creamos la vista y la devolvemos ajax
            
            echo '<table>
                <tr>
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>Vale</th>
                    <th>Fecha</th>
                    <th>Donatario</th>
                    <th>Motivo</th>
                    <th>Cantidad</th>
                    <th>Autorizo</th>
                
                </tr>';
            
                if (mysqli_num_rows($registro)>0) {
                    while($registro2 = mysqli_fetch_array($registro)){
                        echo' <tr>
                                <td>'.$registro2['idLibro'].'</td>
                                <td>'.$registro2['Titulo'].'</td>
                                <td>'.$registro2['Vale'].'</td>
                                <td>'.$registro2['Fecha'].'</td>
                                <td>'.$registro2['Nombre'].'</td>
                                <td>'.$registro2['Motivo'].'</td>
                                <td>'.$registro2['Cantidad'].'</td>
                                <td>'.$registro2['Autorizo'].'</td>
                        
                        
                        </tr>';
                    }
            
                }else{
                    echo '<tr>
                        <td colspan="8">No se encontraron resultados</td>
                    </tr>';
                }
                echo ' </table>'; */
exit;
?>

