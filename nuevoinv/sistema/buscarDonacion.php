<?php
//session_start();
//if ($_SESSION['rol'] !=1 ) {
//    header("location: ../");
//}
include "../conexion.php";

$busqueda ='';
$fechade = '';
$fechaa='';

    if (isset($_REQUEST['busqueda'])&& $_REQUEST['busqueda'] == '') {
        header("location: ListaDonaciones.php");
    }
    if (isset($_REQUEST['fecha_de']) || isset($_REQUEST['fecha_a'])) {
        if ($_REQUEST['fecha_de'] =='' || $_REQUEST['fecha_a']=='') {
             header("location: ListaDonaciones.php");
        }
    }

    if (!empty($_REQUEST['busqueda'])) {
        if (!is_numeric($_REQUEST['busqueda'])) {
            header("location: ListaDonaciones.php ");
        }
        $busqueda = strtolower(mysqli_real_escape_string($conexion,$_REQUEST['busqueda']));
        $where = "idSalida = $busqueda";
        $buscar = "busqueda = $busqueda";
    }

    if (!empty($_REQUEST['fecha_de'])&& !empty($_REQUEST['fecha_a'])) {
        $fechade = mysqli_real_escape_string($conexion, $_REQUEST['fecha_de']);
        $fechaa = mysqli_real_escape_string($conexion,$_REQUEST['fecha_a']);

        $buscar = '';

        if ($fechade > $fechaa) {
            header("location: ListaDonaciones.php");

        }else if ($fechade == $fechaa) {
            $where = "Fecha LIKE '$fechade'";
            $buscar = "fecha_de=$fechade&$fechaa=$fechaa";
        }else{
            //$f_de = $fechade.'00:00:00';
            //$f_a = $fechaa.'23:59:59';
            $where = "Fecha BETWEEN '$fechade' AND '$fechaa'";
            $buscar = "fecha_de=$fechade&fecha_a=$fechaa";
        }
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php include "vistas/scripts.php" ;?>
    <title>Lista de donaciones</title>

    <style>
        .form_serch_date{
	padding: 10px;
	display: flex;
	justify-content: flex-start;
	align-items: center;
	margin: 10px auto;
}
.form_serch_date label{
	margin: 0 10px;
}

.form_serch_date input{
	width: auto;
}
.form_serch_date .btn_view{
	padding: 8px;
}

.btn_view{
	background-color: #5dc262;
	border: 0;
	border-radius: 10px;
	cursor: pointer;
	padding: 10px;
	margin: 0 3px;
	color: white;
}
    </style>
</head>
<body>
<?php require_once "vistas/header.php";?>


<section id="contenedor">
    
    <h1>Lista de donaciones</h1>
    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2) {?>
    <a href="Donacion.php" class="btn_new">Realizar una donación</a>
    <?php }?>
    <form action="buscarDonacion.php" method="get" class="form_buscar">
        <input type="text" name="busqueda" id="busqueda" placeholder="Numero de Vale" value = <?php echo $busqueda  ;?>>
        <input type="submit" value="Buscar" class="btn_buscar" name="" id="">
    </form>

        <div>
        <h5>Buscar por fecha</h5>
        <form action="buscarDonacion.php" method="get" class="form_serch_date">
            <label> de: </label>
            <input type="date" name="fecha_de" id="fecha_de" value ="<?php echo $fechade;  ?>" requiered>
            <label> A </label>
            <input type="date" name="fecha_a" id="fecha_a" value ="<?php echo $fechaa;  ?>" requiered>
            <button type="submit" class="btn_view"> buscar </button>
        
        </form>       
        </div>


    <table>
        <tr>
            <th>Vale</th>
            <th>Fecha</th>
            <th>Donatario</th>
            <th>Autorizó</th>
            <th>Motivo</th>
            <th class="textright">Libros entregados</th>
            <!-- <th>Correo electronico</th> -->
            <!-- <th>Fecha de registro</th> -->
            <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
            <th class="textright">Acciones</th>
            <?php }?>
        </tr>
    <?php
    
    //paginador
    //cuantos registros estan activos
    $sql_registe =mysqli_query($conexion, "SELECT COUNT(*) AS total_registro FROM Salida WHERE $where");

    //guarda el resultado en un array
    $result_register = mysqli_fetch_array($sql_registe);
    //guarda en una variable el numero total de registros
    $total_registro = $result_register['total_registro'];

    //variable que contiene el  numero de registros por pagina
    $por_pagina = 100;

    //validacion de el paginador manda por el url
    if(empty($_GET['pagina']))
    {
        $pagina =1;
    }else{
        $pagina = $_GET['pagina'];
    }

    $desde = ($pagina-1) * $por_pagina;
    $total_paginas = ceil($total_registro / $por_pagina); 

    //el query retorna la informacion el Limit indica desde donde va a iniciar y hasta que regristro va a limitar
        
        //$query =mysqli_query($conexion,"SELECT idDonatario, Nombre, Contacto,Direccion,Telefono, Email,FechaAlta FROM Donatario WHERE Estatus = 1 LIMIT $desde,$por_pagina");

        $query = mysqli_query($conexion,"SELECT s.idSalida,DATE_FORMAT(s.Fecha,'%d/%m/%Y') AS Fecha,CONCAT(u.Nombre,' ',u.Apellido) AS Autorizo, d.Nombre,d.Motivo,s.Total,s.idDonatario 
        FROM Salida s INNER JOIN Usuario u on s.idUsuario = u.idUsuario 
        INNER JOIN Donatario d on s.idDonatario = d.idDonatario 
        WHERE $where
        ORDER BY s.idSalida DESC LIMIT $desde,$por_pagina");



    //mysqli_close($conexion);

    $result =mysqli_num_rows($query);
    if ($result >0) {
        while($data=mysqli_fetch_array($query)){

            ?>
                <tr id ="row_<?php echo $data['idSalida']  ;?> "  >
                    <td><?php echo $data['idSalida']   ;?></td>
                    <td><?php echo $data['Fecha'];  ?></td>
                    <td><?php echo $data['Nombre']   ;?></td>
                    <td><?php echo $data['Autorizo']; ?></td>
                    <td><?php echo $data['Motivo']   ;?></td>
                    <td class="textright"><?php echo $data['Total']   ;?></td>
                    <!-- <td><?php //echo $data['FechaAlta']   ;?></td> -->
                    <?php if ($_SESSION['rol']==1 || $_SESSION['rol']==2 || $_SESSION['rol']==3) {?>
                    <td>
                        <div class="div_acciones">
                            <div>
                                <button class="btn_view view_donacion" type="button" cl="<?php
                                echo $data['idDonatario'] ;?>" f="<?php echo $data['idSalida']  ;?>">Ver</button>
                            </div>
                        </div>
                            <?php }?>
                </tr>
       <?php     
        }


    }


    ?>

    </table>
    <div class="paginador">
        <ul>
            <?php
                if($pagina !=1)
                {
            ?>
            <li><a href="?pagina=<?php echo 1; ?>&<?php echo $buscar  ;?>">|<</a></li>
            <li><a href="?pagina=<?php echo $pagina -1;?>&<?php echo $buscar  ;?>"><<<</a></li>
            <?php
            }
            ?>
            <?php
            for ($i=1; $i <= $total_paginas; $i++) { 
                if($i == $pagina){
                    echo '<li class="pageselected">'.$i.'</li>';
                }else{
                    echo '<li><a href="?pagina='.$i.'&'.$buscar.'">'.$i.'</a></li>';
                }
            }
            ?>
            <?php
            if($pagina !=$total_paginas){
            ?>
            
            <li><a href="?pagina=<?php echo $pagina + 1 ;?>&<?php echo $buscar  ;?>">>>></a></li>
            <li><a href="?pagina=<?php echo $total_paginas; ;?>&<?php echo $buscar  ;?>">>|</a></li>
            <?php
            } 
            ?>

        </ul>
    </div>

</section>
    
</body>
<?php require_once "vistas/footer.php" ?>
</html>